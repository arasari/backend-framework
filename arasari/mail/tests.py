import os

from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.test import TestCase
from tempfile import NamedTemporaryFile
from unittest import mock

from arasari.mail.channels import MailChannel
from arasari.mail.mail import BaseMail, Mail
from arasari.notifications.notification import BaseNotification
from arasari.users.test_utils import ManagesUsers


class Mailable(Mail):
    def build(self): pass


class MailableNotification(BaseNotification):
    def via(self, *args): return ['mail']

    def to_mail(self, *args):
        return Mailable()


class MailChannelTestCase(TestCase, ManagesUsers):
    def test_send_raises_if_no_to_mail_method(self):
        class Notification(BaseNotification):
            def via(self, *args): return ['mail']

        user = self.create_user()
        channel = MailChannel()
        with self.assertRaises(TypeError):
            channel.send(Notification(), user)

    def test_errors_if_to_mail_returns_none(self):
        user = self.create_user()
        channel = MailChannel()
        with mock.patch('arasari.mail.tests.MailableNotification.to_mail', lambda x, y: None):
            with self.assertRaises(ValueError):
                channel.send(MailableNotification(), user)

    def test_send_via_send_mail_if_mailable(self):
        user = self.create_user()
        channel = MailChannel()
        with mock.patch('arasari.mail.channels.send_mail') as send_mail_mock:
            channel.send(MailableNotification(), user)
            send_mail_mock.assert_called_once()

    def test_send_via_django_mail_if_is_mailobject(self):
        user = self.create_user()
        channel = MailChannel()
        with mock.patch('arasari.mail.tests.MailableNotification.to_mail', lambda x, y: EmailMessage()):
            with mock.patch('django.core.mail.EmailMessage.send') as send_mock:
                channel.send(MailableNotification(), user)
                send_mock.assert_called_once()


class BaseMailTestCase(TestCase):
    def test_sets_props(self):
        mail = BaseMail()
        mail.subject = 'subject'
        mail.cc = ['cc', 'cc']
        mail.bcc = ['bcc', 'bcc']
        mail.reply_to = ['reply_to', 'reply_to']
        mail.from_address = 'from_address'
        mail.headers = {
            'Custom-Header': 'custom_value'
        }

        message = mail.as_mail_message()
        self.assertIsInstance(message, EmailMultiAlternatives)
        self.assertEquals(mail.subject, message.subject)
        self.assertEquals(mail.cc, message.cc)
        self.assertEquals(mail.bcc, message.bcc)
        self.assertEquals(mail.reply_to, message.reply_to)
        self.assertEquals(mail.from_address, message.from_email)
        self.assertIn(list(mail.headers.keys())[0], message.extra_headers)

    def test_attaches_file_as_content(self):
        mail = BaseMail()
        mail.attach('file.txt', 'filecontent', 'text/plain')

        message = mail.as_mail_message()
        self.assertEquals(len(message.attachments), 1)
        self.assertEquals(message.attachments[0][0], 'file.txt')
        self.assertEquals(message.attachments[0][1], 'filecontent')
        self.assertEquals(message.attachments[0][2], 'text/plain')

    def test_attaches_file_from_disk(self):
        mail = BaseMail()
        f = NamedTemporaryFile()
        f.write('test'.encode())
        f.seek(0)
        mail.attach_file(f.name, 'application/plain-text')

        message = mail.as_mail_message()
        self.assertEquals(len(message.attachments), 1)
        self.assertEquals(message.attachments[0][0], os.path.basename(f.name))
        self.assertEquals(message.attachments[0][1], 'test'.encode())
        self.assertEquals(message.attachments[0][2], 'application/plain-text')
