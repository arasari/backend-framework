from django.apps import AppConfig


class DefaultConfig(AppConfig):
    name = 'arasari.mail'
    label = 'arasari_mail'
