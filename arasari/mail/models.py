from arasari.mail.mail import BaseMail, send_mail


class Mailable:
    def send_email_message(self, mail: BaseMail):
        """ Send a mailable to this user. """
        send_mail(mail, self.email)
