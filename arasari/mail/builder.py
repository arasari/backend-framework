from django.template.loader import render_to_string
from django.utils.safestring import mark_safe


class BaseBlock:
    def as_html(self):
        raise NotImplemented()

    def as_text(self):
        raise NotImplemented()


class Paragraph(BaseBlock):
    def __init__(self, text, newline=True, weight='inherit', style='normal', color=None, align='left', size=None):
        self.text = text
        self.weight = mark_safe(weight)
        self.style = mark_safe(style)
        self.color = mark_safe(color)
        self.align = mark_safe(align)
        self.size = mark_safe(size)
        self.newline = newline

    def as_html(self):
        tag = 'p' if self.newline else 'span'
        styles = f'font-weight: {self.weight}; font-style: {self.style}'
        classes = []
        if self.color: classes.append(f'text-{self.color}')
        if self.align: classes.append(f'align-{self.align}')
        if self.size: classes.append(f'font-size-{self.size}')
        classes = ' '.join(classes)

        return mark_safe(f'<{tag} class="{classes}" style="{styles}">{self.text}</{tag}>')

    def as_text(self):
        return f'{self.text}\n'


class Heading(BaseBlock):
    def __init__(self, text, level):
        self.text = text
        self.level = level

    def as_html(self):
        return mark_safe(f'<h{self.level}>{self.text}</h{self.level}>')

    def as_text(self):
        return f'\n{self.text}\n'


class Quote(BaseBlock):
    def __init__(self, text):
        self.text = text

    def as_html(self):
        return mark_safe(f'<blockquote>{self.text}</blockquote>')

    def as_text(self):
        text = '\n'.join(map(lambda x: f'>> {x}', self.text.split('\n')))
        return f'\n {text}\n\n'


class Button(BaseBlock):
    def __init__(self, text, url):
        self.text = text
        self.url = url

    def as_html(self):
        template = render_to_string('mail/_button.html', {'text': self.text, 'url': self.url})
        return mark_safe(template)

    def as_text(self):
        return f'{self.url}\n'


class Alert(BaseBlock):
    def __init__(self, text, bg='danger', color='danger'):
        self.text = text
        self.bg = bg
        self.color = color

    def as_html(self):
        return mark_safe(f'<div class="alert bg-{self.bg} text-{self.color}">{self.text}</div>')

    def as_text(self):
        return f'{self.text}\n'
