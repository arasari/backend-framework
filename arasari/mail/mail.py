from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from typing import Union

from arasari.mail.builder import Button, Heading, Paragraph, Quote, Alert


class ConfigurationError(Exception): pass


class BaseMail:
    subject = ''
    preheader = ''
    reply_to = None
    cc = None
    bcc = None
    headers = None
    from_address = getattr(settings, 'DEFAULT_FROM_EMAIL', '')

    _attachments = None
    _file_attachments = None

    def attach(self, filename, content, mime='application/octet-stream'):
        self._attachments = self._attachments or []
        self._attachments.append(
            (filename, content, mime)
        )

    def attach_file(self, path, mime='application/octet-stream'):
        self._file_attachments = self._file_attachments or []
        self._file_attachments.append((path, mime))

    def render_to_html(self):
        return ''

    def render_to_text(self):
        return ''

    def as_mail_message(self) -> EmailMultiAlternatives:
        message = EmailMultiAlternatives(
            subject=self.subject,
            body=self.render_to_text(),
            from_email=self.from_address,
            cc=self.cc,
            bcc=self.bcc,
            attachments=self._attachments,
            reply_to=self.reply_to,
            headers=self.headers,
        )
        message.attach_alternative(self.render_to_html(), 'text/html')

        if self._file_attachments:
            for [path, mime] in self._file_attachments:
                message.attach_file(path, mime)
        return message


class TemplatedMail(BaseMail):
    """ Template-based mail.
    Renders template as mail contents. """

    template = None
    text_template = None

    def get_template_context(self):
        return {k: v for k, v in self.__dict__.items() if not k.startswith('_')}

    def render_to_html(self):
        if not self.template:
            return ''

        return render_to_string(self.template, self.get_template_context())

    def render_to_text(self):
        if not self.text_template:
            return ''

        return render_to_string(self.template, self.get_template_context())


class Mail(BaseMail):
    """ Buildable mail class.
    Builds a message line by line. """

    template = 'mail/_builder.html'
    greeting = ''
    closing = ''
    _is_built = False

    def text(self, text, **kwargs):
        self.lines.append(Paragraph(text, **kwargs))

    def heading(self, text, level):
        self.lines.append(Heading(text, level))

    def button(self, text, url):
        self.lines.append(Button(text, url))

    def quote(self, text):
        self.lines.append(Quote(text))

    def alert(self, text, **kwargs):
        self.lines.append(Alert(text))

    def build(self):
        pass

    def _do_build(self):
        if not hasattr(self, 'lines'):
            self.lines = []

        if not self._is_built:
            self.build()
            self._is_built = True

    def render_to_text(self):
        self._do_build()

        return ''.join(
            [l.as_text() for l in self.lines]
        )

    def render_to_html(self):
        self._do_build()

        return render_to_string(self.template, {
            'greeting': self.greeting,
            'closing': self.closing,
            'preheader': self.preheader,
            'lines': self.lines,
        })


def send_mail(mail: BaseMail, to: Union[str, list, tuple]) -> int:
    """ Sends class-based mail message. """

    if to is not isinstance(to, (list, tuple, set)):
        to = [to, ]

    message = mail.as_mail_message()
    sent = 0
    message.to = to
    message.send()
    sent += 1
    return sent
