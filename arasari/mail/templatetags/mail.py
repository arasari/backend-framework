import base64
import os

from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.inclusion_tag('mail/_button.html')
def button(text, url):
    return {
        'text': text, 'url': url
    }


@register.simple_tag
def public_inline_image(filename, mime='image/png'):
    from django.conf import settings
    full_path = os.path.join(settings.PUBLIC_DIR, filename)

    with open(full_path, 'rb') as f:
        encoded = base64.encodebytes(f.read()).decode()
        return mark_safe(
            f'<img src="data:{mime};base64, {encoded}" />')
