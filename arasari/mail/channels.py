from arasari.mail.mail import BaseMail, send_mail
from arasari.notifications.channels import BaseChannel


class MailChannel(BaseChannel):
    """ Sends notification via e-mail. """

    name = 'mail'

    def send(self, notification, user):
        if not hasattr(notification, 'to_mail'):
            raise TypeError(f'{notification.__class__} must implement "to_mail(self, user) method."')

        mail = notification.to_mail(user)
        if isinstance(mail, BaseMail):
            return send_mail(mail, user.email)

        from django.core.mail import EmailMessage
        if isinstance(mail, EmailMessage):
            return mail.send()

        raise ValueError(
            f'Invalid argument for mail channel: {mail.__class__.__module__}.{mail.__class__.__name__}. '
            f'Must be an instance of {BaseMail.__module__}.{BaseMail.__name__} '
            f'or {EmailMessage.__module__}.{EmailMessage.__name__}.'
        )
