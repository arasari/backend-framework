from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny

from arasari.faq.models import Topic
from arasari.faq.serializers import ShortTopicSerializer, TopicSerializer


class TopicsView(ListAPIView):
    queryset = Topic.objects.filter(is_visible=True)
    serializer_class = ShortTopicSerializer
    permission_classes = (AllowAny,)


class ViewTopicView(RetrieveAPIView):
    queryset = Topic.objects.filter(is_visible=True)
    serializer_class = TopicSerializer
    permission_classes = (AllowAny,)
    lookup_field = 'slug'
