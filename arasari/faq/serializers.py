from rest_framework import serializers

from arasari.faq.models import Topic


class ShortTopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        exclude = ('content', 'created_by')


class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        exclude = ('created_by',)
