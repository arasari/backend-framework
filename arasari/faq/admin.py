from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from arasari.faq.models import Topic


@admin.register(Topic)
class TopicAdmin(SummernoteModelAdmin):
    list_display = (
        'title', 'slug', 'is_visible', 'updated_at',
    )
    list_filter = ('is_visible', 'updated_at')
    search_fields = ('title', 'brief_content', 'content',)
    ordering = ('-created_at',)
    summernote_fields = ('content',)
    prepopulated_fields = {"slug": ("title",)}

    exclude = ['created_by']

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)
