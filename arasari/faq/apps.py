from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DefaultConfig(AppConfig):
    name = 'arasari.faq'
    label = 'arasari_faq'
    verbose_name = _('FAQ')
