from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Topic(models.Model):
    title = models.CharField(_('Title'), max_length=1024)
    slug = models.SlugField(_('Slug'))
    brief_content = models.TextField(_('Brief content'), help_text=_('A short description.'))
    content = models.TextField(_('Content'))
    is_visible = models.BooleanField(_('Is visible?'))
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, verbose_name=_('Created by')
    )
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Updated at'), auto_now=True)

    class Meta:
        verbose_name = _('Topic')
        verbose_name_plural = _('Topics')

    def __str__(self):
        return self.title
