from django.urls import path
from rest_framework.routers import DefaultRouter

from . import views

urlpatterns = [
    path('faq/', views.TopicsView.as_view()),
    path('faq/<slug:slug>/', views.ViewTopicView.as_view()),
]
