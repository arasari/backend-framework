from rest_framework import serializers

from arasari.content.models import Alert, ContentBlock, MenuItem


class AlertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alert
        exclude = ('created_by', 'is_visible')


class ContentBlockSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentBlock
        exclude = ('created_by', 'is_visible')


class MenuItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuItem
        fields = '__all__'
