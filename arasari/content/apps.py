from django.apps import AppConfig
from django.db.models.signals import post_migrate
from django.utils.translation import ugettext_lazy as _


class DefaultConfig(AppConfig):
    name = 'arasari.content'
    label = 'arasari_content'
    verbose_name = _('Content')

    def ready(self):
        from .listeners import create_content_blocks
        post_migrate.connect(create_content_blocks)
