from django.urls import path
from . import views

urlpatterns = [
    path('content/alerts/', views.AlertsView.as_view()),
    path('content/content-blocks/', views.ContentBlockView.as_view()),
    path('content/menu-items/', views.MenuItemsView.as_view()),
]
