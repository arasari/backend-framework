from django.contrib.auth import get_user_model
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _
from django.db import models

User = get_user_model()


class Page(models.Model):
    title = models.CharField(_('title'), max_length=1024, db_index=True)
    slug = models.SlugField(_('slug'), null=False, blank=False)
    content = models.TextField(_('content'), null=True, blank=True)
    is_published = models.BooleanField(_('is published'), default=False,
                                       help_text=_('determines if the page is available to public')
                                       )
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Page')
        verbose_name_plural = _('Pages')


class Alert(models.Model):
    TYPE_INFO = 'info'
    TYPE_DANGER = 'danger'
    TYPE_WARNING = 'warning'
    TYPE_SUCCESS = 'success'

    TYPE_CHOICES = (
        (TYPE_INFO, _('Info')),
        (TYPE_DANGER, _('Danger')),
        (TYPE_WARNING, _('Warning')),
        (TYPE_SUCCESS, _('Success')),
    )

    text = models.TextField(_('Text'))
    title = models.CharField(_('Title'), max_length=512, null=True, blank=True)
    visible_since = models.DateTimeField(
        _('Visible since'), help_text=_('The alert will be visible since this time.'),
        null=True, blank=True
    )
    visible_until = models.DateTimeField(
        _('Visible until'), help_text=_('The alert will be visible until this time.'),
        null=True, blank=True
    )
    is_visible = models.BooleanField(
        _('Is visible?'), default=True, help_text=_('Show or hide the alert.')
    )
    dismissible = models.BooleanField(
        _('Dismissible?'), default=True, help_text=_('Allow users to dismiss the alert.')
    )
    type = models.CharField(_('Type'), max_length=12, choices=TYPE_CHOICES, default=TYPE_INFO)
    created_by = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, verbose_name=_('Created by')
    )
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)

    class Meta:
        verbose_name = _('Alert')
        verbose_name_plural = _('Alerts')

    def __str__(self):
        return self.title or self.text


class ContentBlockPlacement(models.Model):
    id = models.CharField(max_length=128, primary_key=True)
    name = models.CharField(_('Name'), max_length=512)

    def __str__(self):
        return force_text(_(self.name))


class ContentBlock(models.Model):
    name = models.CharField(
        _('Name'), blank=True, null=True, max_length=256, help_text=_('A block name for editors.')
    )
    content = models.TextField(_('Content'), blank=True, null=True)
    placement = models.ForeignKey(
        ContentBlockPlacement, on_delete=models.CASCADE, verbose_name=_('Placement')
    )
    is_visible = models.BooleanField(
        _('Is visible?'), default=False, help_text=_('Show or hide this block.')
    )
    created_by = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, verbose_name=_('Creator')
    )

    class Meta:
        verbose_name = _('Content block')
        verbose_name_plural = _('Content blocks')

    def __str__(self):
        return self.name


class MenuItem(models.Model):
    text = models.CharField(_('Text'), max_length=512)
    icon = models.CharField(_('Icon'), max_length=32, null=True, blank=True)
    new_tab = models.BooleanField(_('Open in new tab'))
    url = models.URLField(_('URL of target page'))

    class Meta:
        verbose_name = _('Menu item')
        verbose_name_plural = _('Menu items')

    def __str__(self):
        return self.text
