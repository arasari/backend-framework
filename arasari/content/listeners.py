from django.db import transaction
from django.db.models.signals import post_migrate
from django.dispatch import receiver

from arasari.content import conf
from arasari.content.models import ContentBlockPlacement


@receiver(post_migrate)
def create_content_blocks(**kwargs):
    known = []
    with transaction.atomic():
        for block in conf.ARASARI_CONTENT_BLOCKS:
            block_id, name = block
            obj, _ = ContentBlockPlacement.objects.get_or_create(pk=block_id)
            obj.name = name
            obj.save()
            known.append(block_id)
        ContentBlockPlacement.objects.exclude(pk__in=known).delete()
