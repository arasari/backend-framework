from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django_summernote.admin import SummernoteModelAdmin

from arasari.content.models import Page, Alert, ContentBlock, MenuItem


@admin.register(Page)
class PageAdmin(SummernoteModelAdmin):
    list_display = (
        'title', 'slug', 'is_published', 'created_at', 'created_at', 'updated_at',
    )
    list_filter = ('is_published', 'created_at', 'updated_at')
    search_fields = ('title', 'content',)
    ordering = ('-created_at',)
    summernote_fields = ('content',)
    prepopulated_fields = {"slug": ("title",)}

    exclude = ['created_by']

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)


@admin.register(Alert)
class AlertAdmin(SummernoteModelAdmin):
    list_display = (
        'title', 'text', 'is_visible', 'visible_since', 'visible_until', 'created_at',
    )
    list_filter = ('is_visible', 'visible_since', 'visible_until', 'created_at')
    search_fields = ('title', 'text')
    ordering = ('-created_at',)
    summernote_fields = ('text',)
    exclude = ['created_by']
    fieldsets = (
        (None, {'fields': ('text', 'type', 'visible_since', 'visible_until', 'title', 'is_visible', 'dismissible')}),
    )

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)


@admin.register(ContentBlock)
class ContentBlockAdmin(SummernoteModelAdmin):
    list_display = ('name', 'placement', 'created_by')
    search_fields = ('name', 'content')
    summernote_fields = ('content',)
    exclude = ['created_by']

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)


@admin.register(MenuItem)
class MenuItemAdmin(ModelAdmin):
    list_display = ('text', 'url', 'new_tab')
    search_fields = ('text', 'url')
