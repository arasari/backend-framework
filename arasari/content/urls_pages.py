from django.urls import path
from . import views

urlpatterns = [
    path('p/<slug:slug>/', views.ViewPageView.as_view())
]
