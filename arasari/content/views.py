from django.db.models import Q
from django.utils import timezone
from django.views.generic import DetailView
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny

from arasari.content.models import Page, Alert, ContentBlock, MenuItem
from arasari.content.serializers import AlertSerializer, ContentBlockSerializer, MenuItemSerializer


class ViewPageView(DetailView):
    template_name = 'pages/view.html'
    model = Page


class AlertsView(ListAPIView):
    serializer_class = AlertSerializer
    queryset = Alert.objects.filter(is_visible=True)
    permission_classes = (AllowAny,)
    pagination_class = None

    def get_queryset(self):
        return self.queryset.filter(
            Q(Q(visible_until__isnull=True) | Q(visible_until__gte=timezone.now())) &
            Q(Q(visible_since__isnull=True) | Q(visible_since__lte=timezone.now()))
        )


class ContentBlockView(ListAPIView):
    serializer_class = ContentBlockSerializer
    queryset = ContentBlock.objects.filter(is_visible=True)
    permission_classes = (AllowAny,)
    pagination_class = None


class MenuItemsView(ListAPIView):
    serializer_class = MenuItemSerializer
    queryset = MenuItem.objects.all()
    permission_classes = (AllowAny,)
    pagination_class = None
