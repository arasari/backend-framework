import hashlib
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from arasari.core.serializers import AvatarField

User = get_user_model()


class BaseUserSerializer(serializers.ModelSerializer):
    """ The default user serializer.
    If you need a custom one set ARASARI_USERS_USER_SERIALIZER setting to point
    to new serializer class."""

    full_name = serializers.CharField(read_only=True)
    phone = serializers.CharField(
        max_length=32, required=True,
        validators=[
            UniqueValidator(queryset=User.objects.all(), message=_('This phone already in use by another user.'))
        ]
    )
    email_verified = serializers.BooleanField(source='is_confirmed', read_only=True)
    display_name = serializers.CharField(read_only=True)
    avatar = serializers.SerializerMethodField()
    avatar_thumbnail = serializers.SerializerMethodField()

    class Meta:
        exclude = ('password', 'confirmation_code', 'confirmed_at')
        read_only_fields = ('last_login', 'date_joined', 'email', 'username',)
        model = User

    def get_avatar(self, obj):
        if obj.avatar:
            return obj.avatar
        return self.build_gravatar_url(obj)

    def get_avatar_thumbnail(self, obj):
        if obj.avatar_thumbnail:
            return obj.avatar_thumbnail
        return self.build_gravatar_url(obj)

    def build_gravatar_url(self, obj):
        hasher = hashlib.md5()
        hasher.update(obj.email.encode())
        hash = hasher.hexdigest()
        return f'//www.gravatar.com/avatar/{hash}?s=200&d=identicon'


class UserSerializer(BaseUserSerializer): pass


class UploadUserPhotoSerializer(serializers.ModelSerializer):
    photo = serializers.ImageField(required=True, allow_null=False, allow_empty_file=False)
    avatar = AvatarField(source='photo')

    class Meta:
        model = User
        fields = ('photo', 'avatar')
