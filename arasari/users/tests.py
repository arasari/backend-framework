import json
import tempfile
from PIL import Image
from django.contrib.auth import get_user_model
from django.core.files import File
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.test import APIClient
from unittest import mock, skip

from .serializers import UserSerializer
from .test_utils import ManagesUsers
from .views import get_user_serializer

User = get_user_model()


class CustomUserSerializer(Serializer): pass


class SerializersTestCase(TestCase):
    def test_get_user_serializer_returns_default(self):
        klass = get_user_serializer()
        self.assertEqual(klass, UserSerializer)

    @mock.patch('arasari.users.conf.ARASARI_USERS_USER_SERIALIZER', 'arasari.users.tests.CustomUserSerializer')
    def test_get_user_serializer_returns_class_from_settings(self):
        klass = get_user_serializer()
        self.assertEqual(klass, CustomUserSerializer)


@skip
class UserViewSetTestCase(TestCase, ManagesUsers):
    def test_list(self):
        user = self.create_user()
        client = APIClient()
        client.force_login(user)
        response: Response = client.get(reverse('user-list'))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_get(self):
        user = self.create_user()
        client = APIClient()
        client.force_login(user)
        response: Response = client.get(reverse('user-detail', kwargs={'pk': user.id}))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_delete(self):
        user = self.create_user()
        user2 = self.create_user()
        client = APIClient()
        client.force_login(user)
        response: Response = client.delete(reverse('user-detail', kwargs={'pk': user2.id}))
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

        response: Response = client.get(reverse('user-detail', kwargs={'pk': user2.id}))
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create(self):
        user = self.create_user()
        client = APIClient()
        client.force_login(user)
        response: Response = client.post(reverse('user-list'), data={
            'phone': self.get_faker().random_int(1000_0000, 9999_9999),
            'email': self.get_faker().email(),
            'password': self.get_faker().email()
        })
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        user = self.create_user()
        client = APIClient()
        client.force_login(user)
        response: Response = client.put(reverse('user-detail', kwargs={'pk': user.id}), data={
            'phone': self.get_faker().random_int(1000_0000, 9999_9999),
            'first_name': self.get_faker().first_name(),
            'last_name': self.get_faker().last_name()
        })
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_partial_update(self):
        user = self.create_user()
        client = APIClient()
        client.force_login(user)
        response: Response = client.patch(reverse('user-detail', kwargs={'pk': user.id}), data={
            'phone': self.get_faker().random_int(1000_0000, 9999_9999),
            'first_name': self.get_faker().first_name(),
            'last_name': self.get_faker().last_name()
        })
        self.assertEquals(response.status_code, status.HTTP_200_OK)


class ProfileViewTestCase(TestCase, ManagesUsers):
    def test_fetches_profile(self):
        user = self.create_user()
        self.client.force_login(user)
        response = self.client.get(reverse('profile'))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        data = json.loads(response.content)
        self.assertEquals(data.get('id'), user.id)

    def test_updates_profile(self):
        user = self.create_user()
        self.client.force_login(user)
        response = self.client.put(reverse('profile'), data={
            'first_name': 'NEW NAME',
            'last_name': 'NEW LASTNAME'
        }, content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        data = json.loads(response.content)
        self.assertIn('user', data)

        user.refresh_from_db()
        self.assertEquals(user.first_name, 'NEW NAME')
        self.assertEquals(user.last_name, 'NEW LASTNAME')


class ProfileImageViewTestCase(TestCase, ManagesUsers):
    def create_image(self):
        image = Image.new('RGB', (200, 200))

        tmp_file = tempfile.NamedTemporaryFile(suffix='.png')
        image.save(tmp_file)
        return tmp_file

    def test_uploads_image(self):
        user = self.create_user()
        self.client.force_login(user)
        response = self.client.patch(reverse('profile_image'), {
            'photo': self.create_image()
        }, content_type='multipart/form-data')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_removes_image(self):
        user = self.create_user(photo=File(self.create_image()))
        self.client.force_login(user)
        response = self.client.delete(reverse('profile_image'))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.assertIsNone(response.data.get('user').get('photo'))
