from django.db import transaction
from django.utils import timezone


class LastSeen:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        self.process_response(request)
        return response

    def process_response(self, request):
        if request.user.is_authenticated:
            with transaction.atomic():
                request.user.last_seen = timezone.now()
                request.user.save()
