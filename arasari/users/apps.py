from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DefaultConfig(AppConfig):
    name = 'arasari.users'
    label = 'arasari_users'
    verbose_name = _('Users')
