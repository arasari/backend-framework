from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from stdimage import StdImageField
from stdimage.validators import MaxSizeValidator, MinSizeValidator

from arasari.core.urls import abs_url
from arasari.mail.models import Mailable
from arasari.notifications.models import Notifiable
from arasari.sms.models import SMSable


def photo_upload_dir(instance, filename):
    return f'users/photos/{instance.id}_{filename}'


class ConfirmsRegistration(models.Model):
    confirmed_at = models.DateTimeField(_('Confirmed at'), null=True, blank=True)
    confirmation_code = models.UUIDField(null=True, blank=True, unique=True, db_index=True)

    class Meta:
        abstract = True

    @property
    def is_confirmed(self):
        return self.confirmed_at is not None

    def confirm_email(self):
        self.confirmed_at = timezone.now()
        self.confirmation_code = None
        self.save()


class BaseUser(AbstractUser, Mailable, Notifiable, SMSable, ConfirmsRegistration):
    """
    A base class for user models.

    All project models must extend this.
    Put other common user fields to this model.
    """

    PHOTO_SIZE = 200
    MAX_PHOTO_SIZE = 1024

    email = models.EmailField(_('email address'), blank=False, unique=True)
    phone = models.CharField(_('Phone'), max_length=64, null=True, blank=True)
    photo = StdImageField(_('photo'), blank=True, null=True, upload_to=photo_upload_dir, variations={
        'thumbnail': {'height': PHOTO_SIZE, 'width': PHOTO_SIZE, 'crop': True}
    }, validators=[MaxSizeValidator(MAX_PHOTO_SIZE, MAX_PHOTO_SIZE), MinSizeValidator(PHOTO_SIZE, PHOTO_SIZE)])
    last_seen = models.DateTimeField(_('last seen'), auto_now=True, null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    @property
    def avatar(self):
        if self.photo:
            return abs_url(self.photo.url)

    @property
    def avatar_thumbnail(self):
        if self.photo:
            return abs_url(self.photo.thumbnail.url)

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    @property
    def display_name(self):
        if self.full_name.strip():
            return self.full_name
        return self.email

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.username:
            self.username = self.email
        return super().save(*args, **kwargs)
