��          �      |      �  #   �          "     &  @   ?     �     �     �     �  ;   �       *          K     l     r     �     �      �     �  	   �     �     �  7   �  #   7     [  .   u  �   �     -  4   <  :   q  4   �  e   �  Z   G  m   �  9   	     J	     c	  (   z	  k   �	  !   
     1
  '   8
     `
     
                                          	                                                  Code was not found in the database. Confirmed at Hi! Hi, %(user.first_name)s! If you did not create an account, no further action is required. Phone Phone Confirmation date Phone has been confirmed. Phone was set. Please click the button below to verify your email address. This email is already taken. This phone already in use by another user. This phone is already confirmed. Users Verify Email Address Verify your email address You have to accept our terms. Your confirmation code: %(code)s email address last seen photo Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-30 02:29+0300
PO-Revision-Date: 2018-11-30 02:30+0300
Last-Translator: Alex Oleshkevich <alex.oleshkevich@gmail.com>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 2.2
 Код не был найден в нашей базе. Дата подтверждения Здравствуйте! Здравствуйте, %(user.first_name)s! Если вы не регистрировались на сайте, то просто проигнорируйте это письмо. Телефон Дата подтверждения телефона Номер телефона был подтвержден. Номер телефона был сохранен. Для подтверждения регистрации пройдите по ссылке ниже. Эта почта уже используется другим пользователем. Этот номер телефона уже используется другим пользователем. Этот номер уже был подтвержден. Пользователи Подтвердить Подтвердите ваш адрес Необходимо принять условия пользовательского соглашения. Код проверки: %(code)s e-mail последняя активность фото 