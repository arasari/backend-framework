from django.urls import path

from . import views

urlpatterns = [
    path('profile/image/', views.ProfileImageView.as_view(), name='profile_image'),
    path('profile/', views.ProfileView.as_view(), name='profile'),
]
