from django.conf import settings

ARASARI_USERS_USER_SERIALIZER = getattr(
    settings, 'ARASARI_USERS_USER_SERIALIZER', 'arasari.users.serializers.UserSerializer'
)
