from django.contrib.auth import get_user_model

from arasari.core.test_utils import ProvidesFaker

User = get_user_model()


class ManagesUsers(ProvidesFaker):
    """ User management tools for testing purposes. """

    def create_user(self, **kwargs) -> User:
        """ Creates a new user. """
        fake = self.get_faker()

        if 'email' not in kwargs:
            kwargs['email'] = fake.email()
            if 'username' not in kwargs:
                kwargs['username'] = kwargs['email']

        if 'password' not in kwargs:
            kwargs['password'] = 'password'

        if 'username' not in kwargs:
            kwargs['username'] = fake.user_name()

        user = User(**kwargs)
        user.set_password(kwargs['password'])
        user.save()
        return user
