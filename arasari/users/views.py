from django.contrib.auth import get_user_model
from importlib import import_module
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from arasari.users.serializers import (UploadUserPhotoSerializer)
from . import conf

User = get_user_model()


def get_user_serializer():
    """ Returns a name of a serializer used with User model.
         It will look into USERS_USER_SERIALIZER setting
         which is a full name of the serializer class.
         If it is not defined, it will use default serializer shipped with this package.
     """
    klass_name = conf.ARASARI_USERS_USER_SERIALIZER
    module_name, *rest, package_name = klass_name.rpartition('.')
    klass = getattr(import_module(module_name), package_name)
    return klass


class ProfileView(GenericAPIView):
    serializer_class = get_user_serializer()
    queryset = User.objects.all()

    def get(self, request):
        return Response(self.get_serializer(request.user).data)

    def put(self, request):
        serializer = self.get_serializer(request.user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({
            'message': 'updated',
            'user': serializer.data
        })


class ProfileImageView(GenericAPIView):
    serializer_class = UploadUserPhotoSerializer
    queryset = User.objects.all()

    def patch(self, request):
        """ Upload an user photo.
         There request must contain "photo" field.
         """
        serializer = self.get_serializer(instance=request.user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({
            'message': 'ok',
            'user': serializer.data
        })

    def delete(self, request):
        """ Remove user's photo. """
        request.user.photo = None
        request.user.save()
        return Response({
            'message': 'ok',
            'user': self.get_serializer(instance=request.user).data
        }, status=status.HTTP_200_OK)
