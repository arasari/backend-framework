from importlib import import_module


def import_class(class_name):
    """ Import class using FQCN. """

    modname, *rest, klass = class_name.rpartition('.')
    module = import_module(modname)
    if not hasattr(module, klass):
        raise TypeError(f'Class {klass} not found in {modname}.')
    return getattr(module, klass)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
