from django import template

from arasari.core.urls import abs_url

register = template.Library()


@register.simple_tag()
def public_path(path):
    from django.conf import settings
    return settings.public_root(path)


@register.filter()
def abs_path(value):
    return abs_url(value)
