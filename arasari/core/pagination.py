from collections import OrderedDict

from rest_framework.pagination import PageNumberPagination as BasePageNumberPagination
from rest_framework.response import Response


class PageNumberPagination(BasePageNumberPagination):
    """ This paginator shows extra information.
    Each response includes:
        `total` - total matched items
        `next` - next URL
        `previous` - prev URL
        `per_page` - items per page
        `current_page` - current page
        `results` - fetched items
    """
    page_size_query_param = 'page_size'
    max_page_size = 100

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('total', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('per_page', self.page_size),
            ('current_page', self.page.number),
            ('results', data)
        ]))
