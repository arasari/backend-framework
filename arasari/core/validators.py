import requests
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from .conf import RECAPTCHA_SECRET


class ReCaptchaValidator:
    VALIDATE_ENDPOINT = 'https://www.google.com/recaptcha/api/siteverify'

    ERRORS = {
        'missing-input-secret':   'The secret parameter is missing.',
        'invalid-input-secret':   'The secret parameter is invalid or malformed.',
        'missing-input-response': 'The response parameter is missing.',
        'invalid-input-response': 'The response parameter is invalid or malformed.',
        'bad-request':            'The request is invalid or malformed.',
        'timeout-or-duplicate':   'The request has timed out or duplicate.',
    }

    def __call__(self, value):
        response = requests.post(self.VALIDATE_ENDPOINT, data={
            'secret':   RECAPTCHA_SECRET,
            'response': value,
        })

        if response.status_code != 200:
            raise serializers.ValidationError(_('HTTP error happened during reCAPTCHA validation.'))

        response = response.json()
        if response.get('success') is not True:
            first_error = response.get('error-codes')[0]
            raise serializers.ValidationError(self.ERRORS[first_error])
        return True
