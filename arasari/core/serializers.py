import hashlib
from rest_framework import serializers

from arasari.core.utils import get_client_ip
from arasari.core.validators import ReCaptchaValidator


class ReCaptchaField(serializers.CharField):
    """ ReCAPTCHA field. """

    def __init__(self, **kwargs):
        kwargs['write_only'] = True
        super().__init__(**kwargs)

        self.validators.append(ReCaptchaValidator())


class AvatarField(serializers.ReadOnlyField):
    """ Returns abs URL to user photo or fallbacks to Gravatar URL. """

    def to_representation(self, value):
        request = self.context['request']
        if not value:
            email = request.user.email
            hasher = hashlib.md5()
            hasher.update(email.encode())
            hash = hasher.hexdigest()
            return f'//www.gravatar.com/avatar/{hash}?s=200&d=identicon'
        return request.build_absolute_uri(value.url)


class ClientIPDefault(object):
    def set_context(self, serializer_field):
        self.ip = get_client_ip(serializer_field.context['request'])

    def __call__(self):
        return self.ip
