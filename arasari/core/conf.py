from django.conf import settings

RECAPTCHA_SECRET = getattr(settings, 'RECAPTCHA_SECRET', None)
RECAPTCHA_SITEKEY = getattr(settings, 'RECAPTCHA_SITEKEY', None)

APP_NAME = getattr(settings, 'APP_NAME', '')
APP_URL = getattr(settings, 'APP_URL', '')
