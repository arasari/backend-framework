from django.urls import reverse

from . import conf


def abs_reverse(viewname, app_url=conf.APP_URL, *args, **kwargs):
    """Reverses route name into absolute URL."""

    url = reverse(viewname, *args, **kwargs)
    return app_url + url


def abs_url(relative_url: str) -> str:
    """ Prepends domain name to the relative URL. """
    return '/'.join([conf.APP_URL, relative_url.strip('/')])
