from django.apps import AppConfig


class DefaultConfig(AppConfig):
    name = 'arasari.core'
    label = 'arasari_core'
