from rest_framework import status
from rest_framework.views import exception_handler


def formatted_exception_handler(exc, context):
    """ This exception handler renames fields.
     "detail" -> "message"
     "non_field_errors" -> merged into "detail" (replaces if existed)
     "errors" -> contains validation errors
     """
    response = exception_handler(exc, context)
    if response is not None:
        if response.status_code >= status.HTTP_400_BAD_REQUEST:
            data = {}
            if 'detail' in response.data:
                data['message'] = response.data.get('detail')
                del response.data['detail']

            if 'non_field_errors' in response.data:
                data['message'] = ' '.join(response.data.get('non_field_errors'))
                del response.data['non_field_errors']

            data['errors'] = response.data
            response.data = data
        return response
    return None


class ConfigurationError(Exception): pass
