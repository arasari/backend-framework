import os
from django.http import StreamingHttpResponse, HttpResponse

from django.views import View


class VersionView(View):
    """ Displays build version information.

    Usage:
        in urls.py

        urlpatterns = [
            path('version', metro.core.VersionView.as_view(version_file='/path/to/version/file'))
        ]
    """

    # Path to a plain text file containing the version information.
    version_file = None

    def get(self, *args):
        if not self.version_file:
            raise Exception('VersionView: "version_file" is not set. The value must be a valid path.')

        if not os.path.exists(self.version_file):
            return HttpResponse(f'VersionView: the version file does not exist: "{self.version_file}"', 404)

        return StreamingHttpResponse(open(self.version_file))
