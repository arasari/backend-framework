""" The module exports common utilities and helpers for use with unittests."""
from faker import Faker

fake = Faker()


class ProvidesFaker:
    """ Provides Faker instance for sample data generation. """

    def get_faker(self) -> Faker:
        """ Returns Faker instance. """
        return fake

    @property
    def faker(self) -> Faker:
        return fake
