from . import conf


def core():
    return {
        'APP_NAME': conf.APP_NAME,
        'APP_URL': conf.APP_URL,
    }
