import re
import types
from django.core.management import BaseCommand, CommandParser
from django.conf import settings
from django.urls import URLResolver
from importlib import import_module
from texttable import Texttable


def show_urls(urllist, depth, routes):
    for entry in urllist:
        if isinstance(entry, (URLResolver,)):
            routes.append(('group', entry.pattern, '', entry.callback))
        else:
            routes.append(('route', entry.pattern, entry.name or '', entry.callback))

        if hasattr(entry, 'url_patterns'):
            show_urls(entry.url_patterns, depth + 1, routes)


def get_view_name(view):
    if isinstance(view, (types.FunctionType)):
        return f'{view.__module__}.{view.__name__}'
    return view.__class__


class Command(BaseCommand):
    def add_arguments(self, parser: CommandParser):
        parser.add_argument('--search', '-s')

    def handle(self, search, **kwargs):
        routes = []
        urls_module = import_module(settings.ROOT_URLCONF)
        show_urls(urls_module.urlpatterns, 0, routes)

        table = Texttable()
        table.add_row(('Type', 'Pattern', 'View', 'Name'))
        table.set_cols_width((5, 30, 50, 30))

        for route in routes:
            type, pattern, name, callback = route

            if search and not search in str(pattern) and not search in name: continue

            table.add_row((type, pattern, get_view_name(callback), name))

        self.stdout.write(table.draw())
