from arasari.core.utils import import_class
from .conf import NOTIFICATION_CHANNELS

_channels = None


class ConfigurationError(ValueError): pass


def get_channels():
    global _channels
    if not _channels:
        _channels = {}
        for channel in NOTIFICATION_CHANNELS:
            channel_class = import_class(channel)
            if not channel_class.name:
                raise ConfigurationError(f'Channel {channel_class.__name__} must define "name" attribute.')

            if channel_class.name in _channels:
                raise ConfigurationError(
                    f'Channel name "{channel_class.name}" is already registered (seen in {channel_class.__name__})'
                )
            _channels[channel_class.name] = channel_class()
    return _channels


class BaseChannel:
    name = None

    def send(self, notification, user):
        raise NotImplementedError()


def get_channel(name: str) -> BaseChannel:
    channels = get_channels()
    if not name in channels:
        raise ConfigurationError(
            f'Channel {name} is not registered in NOTIFICATION_CHANNELS setting.'
        )
    return channels[name]
