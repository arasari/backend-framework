from .notification import BaseNotification, send_notification


class Notifiable:
    """ Enhances user with notification system. """

    def send_notification(self, notification: BaseNotification, only=None):
        """ Dispatch a notification to this user. """

        return send_notification(notification, self, only)
