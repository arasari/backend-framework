from django.test import TestCase
from unittest import mock

from arasari.notifications.channels import BaseChannel, ConfigurationError, get_channel, get_channels
from arasari.notifications.notification import BaseNotification, send_notification, DeliveryError
from arasari.notifications.signals import notification_send_success, notification_send_failure
from arasari.users.test_utils import ManagesUsers

NOTIFICATION_CHANNELS = [
    'arasari.notifications.tests.SampleChannel',
    'arasari.notifications.tests.Sample2Channel',
    'arasari.notifications.tests.DeliveryErrorChannel',
    'arasari.notifications.tests.FailingChannel',
]


class SampleChannel(BaseChannel):
    name = 'sample'

    #
    def send(self, notification, user):
        pass


class Sample2Channel(BaseChannel):
    name = 'sample2'

    def send(self, notification, user):
        pass


class DeliveryErrorChannel(BaseChannel):
    name = 'delivery'

    def send(self, notification, user):
        raise DeliveryError('error')


class FailingChannel(BaseChannel):
    name = 'fail'

    def send(self, notification, user):
        raise TypeError('error')


class NamelessSampleChannel(BaseChannel): pass


class GetChannelsTestCase(TestCase):
    @mock.patch('arasari.notifications.channels._channels', None)
    def test_ok(self):
        with mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', NOTIFICATION_CHANNELS):
            channels = get_channels()
            self.assertIn('sample', channels)

    @mock.patch('arasari.notifications.channels._channels', None)
    def test_fails_if_channel_has_no_name(self):
        settings = [
            'arasari.notifications.tests.NamelessSampleChannel',
        ]
        with mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', settings):
            with self.assertRaises(ConfigurationError):
                get_channels()

    @mock.patch('arasari.notifications.channels._channels', None)
    def test_fails_if_channel_is_duplicate(self):
        settings = [
            'arasari.notifications.tests.SampleChannel',
            'arasari.notifications.tests.SampleChannel',
        ]
        with mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', settings):
            with self.assertRaises(ConfigurationError):
                get_channels()


class GetChannelTestCase(TestCase):
    @mock.patch('arasari.notifications.channels._channels', None)
    def test_ok(self):
        with mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', NOTIFICATION_CHANNELS):
            channel = get_channel('sample')
            self.assertIsInstance(channel, SampleChannel)

    @mock.patch('arasari.notifications.channels._channels', None)
    def test_errors_if_notfound(self):
        with mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', NOTIFICATION_CHANNELS):
            with self.assertRaises(ConfigurationError):
                get_channel('sample1')


class SendNotificationTestCase(TestCase, ManagesUsers):

    @mock.patch('arasari.notifications.channels._channels', None)
    def test_ok_for_single_channel(self):
        user = self.create_user()

        class Notification(BaseNotification):
            via = ['sample']

        def _signal_handler(*args, **kwargs): pass

        with mock.patch.object(_signal_handler, '__call__') as _signal_handler_mock:
            notification_send_success.connect(_signal_handler_mock)

            with mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', NOTIFICATION_CHANNELS):
                with mock.patch('arasari.notifications.tests.SampleChannel.send') as send_mock:
                    result = send_notification(Notification(), user)
                    send_mock.assert_called_once()
                    self.assertIn('sample', result)

            _signal_handler_mock.assert_called_once()
            notification_send_success.disconnect(_signal_handler_mock)

    @mock.patch('arasari.notifications.channels._channels', None)
    @mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', NOTIFICATION_CHANNELS)
    def test_ok_for_multiple_channels(self):
        user = self.create_user()

        class Notification(BaseNotification):
            via = ['sample', 'sample2']

        with mock.patch('arasari.notifications.tests.SampleChannel.send') as send_mock:
            with mock.patch('arasari.notifications.tests.Sample2Channel.send') as send_mock2:
                result = send_notification(Notification(), user)
                send_mock.assert_called_once()
                send_mock2.assert_called_once()

                self.assertIn('sample', result)
                self.assertIn('sample2', result)

    @mock.patch('arasari.notifications.channels._channels', None)
    @mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', NOTIFICATION_CHANNELS)
    def test_handles_delivery_error_if_notification_fails(self):
        user = self.create_user()

        class Notification(BaseNotification):
            via = ['delivery']

        def _signal_handler(*args, **kwargs): pass

        with mock.patch.object(_signal_handler, '__call__') as _signal_handler_mock:
            notification_send_failure.connect(_signal_handler_mock)

            result = send_notification(Notification(), user)
            self.assertIn('delivery', result)
            self.assertFalse(result['delivery'][0])
            self.assertIsInstance(result['delivery'][1], DeliveryError)

            _signal_handler_mock.assert_called_once()
            notification_send_failure.disconnect(_signal_handler_mock)

    @mock.patch('arasari.notifications.channels._channels', None)
    @mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', NOTIFICATION_CHANNELS)
    def test_propagates_unrecoverable_error(self):
        user = self.create_user()

        class Notification(BaseNotification):
            via = ['fail']

        def _signal_handler(*args, **kwargs): pass

        with mock.patch.object(_signal_handler, '__call__') as _signal_handler_mock:
            notification_send_failure.connect(_signal_handler_mock)

            with self.assertRaises(TypeError):
                result = send_notification(Notification(), user)

    @mock.patch('arasari.notifications.channels._channels', None)
    @mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', NOTIFICATION_CHANNELS)
    def test_via_is_callable(self):
        user = self.create_user()

        class Notification(BaseNotification):
            def via(self, user): return ['sample']

        notification = Notification()

        with mock.patch.object(notification, 'via') as via_mock:
            send_notification(notification, user)
        via_mock.assert_called_once()

    @mock.patch('arasari.notifications.channels._channels', None)
    @mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', NOTIFICATION_CHANNELS)
    def test_via_is_iterable(self):
        user = self.create_user()

        class Notification(BaseNotification):
            via = ['sample', 'sample2']

        result = send_notification(Notification(), user)
        self.assertIn('sample', result)
        self.assertIn('sample2', result)

    @mock.patch('arasari.notifications.channels._channels', None)
    @mock.patch('arasari.notifications.channels.NOTIFICATION_CHANNELS', NOTIFICATION_CHANNELS)
    def test_respects_only_argument(self):
        user = self.create_user()

        class Notification(BaseNotification):
            via = ['sample', 'sample2']

        result = send_notification(Notification(), user, only=['sample2'])
        self.assertNotIn('sample', result)
        self.assertIn('sample2', result)
