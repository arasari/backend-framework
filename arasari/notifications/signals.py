from django.core import signals

notification_send_success = signals.Signal(providing_args=['notification', 'user', 'results', 'channel'])
notification_send_failure = signals.Signal(providing_args=['notification', 'user', 'exception', 'channel_name'])
