from arasari.notifications.channels import get_channel
from arasari.notifications.signals import notification_send_success, notification_send_failure


class BaseNotification:
    via = []
    """ Defines a list of channel names to use for sending. """


class DeliveryError(Exception):
    """ Tells that there was a recoverable error while sending the notification. """
    pass


def send_notification(notification: BaseNotification, user, only=None):
    """ Send notification to user.

    Returns a dict where keys are channel name and values are results of the operation.
    """

    if callable(notification.via):
        notification_channels = notification.via(user)
    else:
        notification_channels = notification.via

    if only:
        notification_channels = list(
            filter(lambda x: x in only, notification_channels)
        )

    results = {}
    for channel_name in notification_channels:
        try:
            channel = get_channel(channel_name)
            result = channel.send(notification, user)
            results[channel_name] = (True, result)
            notification_send_success.send(
                __name__, notification=notification, user=user, results=results, channel=channel
            )
        except DeliveryError as ex:
            results[channel_name] = (False, ex)
            notification_send_failure.send(
                __name__, notification=notification, user=user, exception=ex, channel_name=channel_name
            )
        except Exception as ex:
            notification_send_failure.send(
                __name__, notification=notification, user=user, exception=ex, channel_name=channel_name
            )
            raise ex
    return results
