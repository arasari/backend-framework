from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DefaultConfig(AppConfig):
    name = 'arasari.notifications'
    label = 'arasari_notifications'
    verbose_name = _('Notifications')
