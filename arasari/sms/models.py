from arasari.sms.messages import SmsMessage


class SMSable:
    """ Enhances user model with SMS capabilities. """

    def send_sms(self, message: SmsMessage, test: bool = False):
        from arasari.sms.providers import send_sms_message
        send_sms_message(message, [self.phone], test)
