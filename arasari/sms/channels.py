from arasari.notifications.channels import BaseChannel
from arasari.sms.messages import SmsMessage


class SmsChannel(BaseChannel):
    """ SMS notification channel. """

    name = 'sms'

    def send(self, notification, user):
        if not hasattr(notification, 'to_sms'):
            raise TypeError(f'{notification.__class__} must implement "to_sms(self, user) method."')

        message = notification.to_sms(user)
        if isinstance(message, SmsMessage):
            return user.send_sms(message)

        raise ValueError(
            f'Invalid argument for SMS channel: {message.__class__.__module__}.{message.__class__.__name__}. '
            f'Must be an instance of {SmsMessage.__module__}.{SmsMessage.__name__} '
        )
