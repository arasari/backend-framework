from django.contrib.auth import get_user_model
from requests import Response
from unittest import TestCase, mock
from unittest.mock import MagicMock

from arasari.sms.channels import SmsChannel
from arasari.sms.messages import SmsMessage
from arasari.sms.models import SMSable
from arasari.sms.providers import WebSmsBy, BaseProvider, get_sms_provider, send_sms_message, DispatchResult
from arasari.sms.signals import sms_send_success, sms_send_failure


class TestNotification:
    def to_sms(self, user):
        return SmsMessage('text', 'sender')


class TestInvalidNotification: pass


class TestNotificationNoMessage:
    def to_sms(self, user):
        return object()


class UserStub:
    phone = '123'


class TestProvider(BaseProvider):
    def send(self, **kwargs):
        return True


class TestFailingProvider(BaseProvider):
    def send(self, **kwargs):
        raise TypeError('error')


class TestFactoryProvider(BaseProvider):
    def factory(self): pass


class SmsableUserStub(SMSable, UserStub): pass


class SmsChannelTestCase(TestCase):
    def test_send_ok(self):
        user = SmsableUserStub()
        channel = SmsChannel()
        notification = TestNotification()

        with mock.patch.object(user, 'send_sms') as send_sms_mock:
            channel.send(notification, user)
        send_sms_mock.assert_called_once()

    def test_send_fails_if_to_sms_method_missed(self):
        user = SmsableUserStub()
        channel = SmsChannel()
        notification = TestInvalidNotification()

        with self.assertRaises(TypeError):
            channel.send(notification, user)

    def test_send_fails_if_to_sms_returns_not_smsmessage_instance(self):
        user = SmsableUserStub()
        channel = SmsChannel()
        notification = TestNotificationNoMessage()

        with self.assertRaises(ValueError):
            channel.send(notification, user)


class SmsMessageTestCase(TestCase):
    def test_init_accepts_text_and_sender(self):
        message = SmsMessage('text', 'sender')
        self.assertEqual(message.text, 'text')
        self.assertEqual(message.sender, 'sender')

    @mock.patch('arasari.sms.messages.ARASARI_SMS_SENDER', 'defaultsender')
    def test_init_has_sender_optional(self):
        message = SmsMessage('text')
        self.assertEqual(message.text, 'text')
        self.assertEqual(message.sender, 'defaultsender')


class SMSableTestCase(TestCase):
    def test_has_method(self):
        obj = SmsableUserStub()
        self.assertTrue(hasattr(obj, 'send_sms'))

    @mock.patch('arasari.sms.providers.send_sms_message')
    def test_sends_sms(self, send_sms_message_mock: MagicMock):
        user = SmsableUserStub()
        message = SmsMessage('text')
        user.send_sms(message, False)
        send_sms_message_mock.assert_called_once_with(message, [user.phone], False)


class WebSmsByProviderTestCase(TestCase):
    def test_send_success(self):
        provider = WebSmsBy('username', 'apikey')
        message = SmsMessage('text')

        success_response = Response()
        success_response.status_code = 200
        success_response._content = b'{"status": "success"}'
        with mock.patch('requests.get', lambda x, y: success_response):
            result = provider.send(message, ['123'])
            self.assertTrue(result.status)

    def test_send_fails_if_not_200(self):
        provider = WebSmsBy('username', 'apikey')
        message = SmsMessage('text')

        success_response = Response()
        success_response.status_code = 400
        with mock.patch('requests.get', lambda x, y: success_response):
            result = provider.send(message, ['123'])
            self.assertFalse(result.status)

    def test_send_fails_if_status_not_success(self):
        provider = WebSmsBy('username', 'apikey')
        message = SmsMessage('text')

        success_response = Response()
        success_response.status_code = 200
        success_response._content = b'{"status": "error", "message": "error_message"}'
        with mock.patch('requests.get', lambda x, y: success_response):
            result = provider.send(message, ['123'])
            self.assertFalse(result.status)
            self.assertEqual(result.message, 'error_message')

    @mock.patch('arasari.sms.conf.ARASARI_SMS_WEBSMSBY_USERNAME', 'username')
    @mock.patch('arasari.sms.conf.ARASARI_SMS_WEBSMSBY_APIKEY', 'apikey')
    def test_factory_returns_instance(self):
        instance = WebSmsBy.factory()
        self.assertEqual(instance.username, 'username')
        self.assertEqual(instance.apikey, 'apikey')


class GetSmsProviderTestCase(TestCase):
    def test_returns_instance_of_baseprovider(self):
        instance = get_sms_provider('arasari.sms.tests.TestProvider')
        self.assertIsInstance(instance, TestProvider)

    def test_call_factory_if_provides_supports_it(self):
        with mock.patch('arasari.sms.tests.TestFactoryProvider.factory') as factory_mock:
            get_sms_provider('arasari.sms.tests.TestFactoryProvider')
            factory_mock.assert_called_once()


default_test_provider = TestProvider()
failing_test_provider = TestFailingProvider()


class SendSmsMessageTestCase(TestCase):
    @mock.patch('arasari.sms.providers.get_sms_provider', lambda x: default_test_provider)
    def test_send_success_via_default_provider(self):
        def _signal_handler(*args, **kwargs): pass

        with mock.patch.object(_signal_handler, '__call__') as _signal_handler_mock:
            sms_send_success.connect(_signal_handler_mock)

            with mock.patch.object(default_test_provider, 'send') as send_mock:
                send_sms_message(SmsMessage('test'), ['123'])
            send_mock.assert_called_once()

            _signal_handler_mock.assert_called_once()
            sms_send_success.disconnect(_signal_handler_mock)

    @mock.patch('arasari.sms.providers.get_sms_provider', lambda x: failing_test_provider)
    def test_send_handles_exception(self):
        def _signal_handler(*args, **kwargs): pass

        with mock.patch.object(_signal_handler, '__call__') as _signal_handler_mock:
            sms_send_failure.connect(_signal_handler_mock)

            with self.assertRaises(TypeError):
                send_sms_message(SmsMessage('test'), ['123'])

            _signal_handler_mock.assert_called_once()
            sms_send_failure.disconnect(_signal_handler_mock)
