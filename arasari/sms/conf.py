from django.conf import settings

ARASARI_SMS_PROVIDER = getattr(settings, 'ARASARI_SMS_PROVIDER', None)
ARASARI_SMS_SENDER = getattr(settings, 'ARASARI_SMS_SENDER', None)
ARASARI_SMS_WEBSMSBY_USERNAME = getattr(settings, 'ARASARI_SMS_WEBSMSBY_USERNAME', None)
ARASARI_SMS_WEBSMSBY_APIKEY = getattr(settings, 'ARASARI_SMS_WEBSMSBY_APIKEY', None)
