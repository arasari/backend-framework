from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DefaultConfig(AppConfig):
    name = 'arasari.sms'
    label = 'arasari_sms'
    verbose_name = _('SMS')

    def ready(self):
        # noinspection PyUnresolvedReferences
        from . import checks
