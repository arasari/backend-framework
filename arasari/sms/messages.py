from .conf import ARASARI_SMS_SENDER


class SmsMessage:
    """ Represents SMS message. """

    def __init__(self, text, sender=None):
        self.text = text
        self.sender = sender or ARASARI_SMS_SENDER
