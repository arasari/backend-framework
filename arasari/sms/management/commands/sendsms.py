from django.core.management import BaseCommand, CommandParser

from arasari.sms.messages import SmsMessage
from arasari.sms.providers import get_sms_provider
from ...conf import ARASARI_SMS_PROVIDER


class Command(BaseCommand):
    def add_arguments(self, parser: CommandParser):
        parser.add_argument('recipients')
        parser.add_argument('message')
        parser.add_argument('--provider', '-p', default=ARASARI_SMS_PROVIDER)
        parser.add_argument('--test', '-t', action='store_true', default=False)

    def handle(self, recipients, message, test, provider, *args, **options):
        self.stdout.write(f'Using {provider}')
        sms = get_sms_provider(provider)
        result = sms.send(SmsMessage(message), recipients.split(','), test=test)
        self.stdout.write(f'Send result: {result}', )
