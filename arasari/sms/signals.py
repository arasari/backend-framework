from django.dispatch import Signal

sms_send_success = Signal(providing_args=['message', 'recipients', 'result', 'test'])
sms_send_failure = Signal(providing_args=['message', 'recipients', 'exception', 'test'])
