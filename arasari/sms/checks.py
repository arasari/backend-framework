from django.core.checks import Error, Warning, register

from . import conf


@register()
def check_for_sender_name(app_configs, **kwargs):
    errors = []
    if not conf.ARASARI_SMS_SENDER:
        errors.append(
            Warning(
                'ARASARI_SMS_SENDER setting was not found',
                hint='Set ARASARI_SMS_SENDER in settings.py',
                id='arasari.sms.E001',
            )
        )

    if not conf.ARASARI_SMS_PROVIDER:
        errors.append(
            Error(
                'ARASARI_SMS_PROVIDER setting was not found',
                hint='Set ARASARI_SMS_PROVIDER in settings.py',
                id='arasari.sms.E002',
            )
        )

    return errors


@register()
def check_websmsby_settings(*args, **kwargs):
    errors = []
    if conf.ARASARI_SMS_PROVIDER == 'arasari.sms.providers.WebSmsBy':
        if not conf.ARASARI_SMS_WEBSMSBY_USERNAME:
            errors.append(
                Error(
                    'ARASARI_SMS_WEBSMSBY_USERNAME setting was not found',
                    hint='Set ARASARI_SMS_WEBSMSBY_USERNAME in settings.py',
                    id='arasari.sms.E003',
                )
            )

        if not conf.ARASARI_SMS_WEBSMSBY_APIKEY:
            errors.append(
                Error(
                    'ARASARI_SMS_WEBSMSBY_APIKEY setting was not found',
                    hint='Set ARASARI_SMS_WEBSMSBY_APIKEY in settings.py',
                    id='arasari.sms.E004',
                )
            )
    return errors
