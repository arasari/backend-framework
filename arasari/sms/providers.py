import requests
from typing import Union

from arasari.core.utils import import_class
from arasari.sms import conf
from arasari.sms.messages import SmsMessage
from arasari.sms.signals import sms_send_failure, sms_send_success


class DispatchResult:
    """ Represents SMS dispatch result.  """

    def __init__(self, status: bool, message=''):
        self.status = status
        self.message = message

    def __repr__(self):
        return f'{self.__class__}: status: {self.status}, message: "{self.message}"'


class BaseProvider:
    def send(self, message: SmsMessage, recipients: Union[list, tuple, set, str], test: bool = False):
        raise NotImplemented


class Dummy(BaseProvider):
    """ Dummy provides. Does nothing. """

    def send(self, message: SmsMessage, recipients: Union[list, tuple, set, str], test: bool = False):
        return DispatchResult(True)


class Console(BaseProvider):
    """ Prints message details to console. Does not send anything. """

    def send(self, message: SmsMessage, recipients: Union[list, tuple, set, str], test: bool = False):
        print(f'SMS: from={message.sender} to={" ,".join(recipients)} message="{message.text}", text="{test}"')
        return DispatchResult(True)


class WebSmsBy(BaseProvider):
    """ Provider for websms.by service.
    Requires ARASARI_SMS_WEBSMSBY_USERNAME and ARASARI_SMS_WEBSMSBY_APIKEY extra settings.

    """
    API_ENDPOINT = 'https://cp.websms.by/'

    def __init__(self, username, apikey):
        self.username = username
        self.apikey = apikey

    def send(self, message: SmsMessage, recipients: Union[list, tuple, set, str], test: bool = False):
        """
        Send SMS message via this provider.
        :param message:
        :param recipients:
        :param test:
        :return:
        """
        if isinstance(recipients, str):
            recipients = [recipients]

        response = requests.get(self.API_ENDPOINT, {
            'r': 'api/msg_send',
            'user': self.username,
            'apikey': self.apikey,
            'recipients': ','.join(recipients),
            'message': message.text,
            'sender': message.sender,
            'test': 1 if test else 0
        })
        if not response.status_code == 200:
            return DispatchResult(False, f'Invalid status code: {response.status_code}')

        result = response.json()
        if result['status'] != 'success':
            return DispatchResult(False, result['message'])
        return DispatchResult(True)

    @staticmethod
    def factory():
        from . import conf
        return WebSmsBy(conf.ARASARI_SMS_WEBSMSBY_USERNAME, conf.ARASARI_SMS_WEBSMSBY_APIKEY)


def get_sms_provider(provider_class) -> BaseProvider:
    """ Returns configured SMS provider. """

    klass = import_class(provider_class)
    if hasattr(klass, 'factory'):
        return klass.factory()
    return klass()


def send_sms_message(message, recipients, test=False, provider_class=conf.ARASARI_SMS_PROVIDER):
    """
    Sends SMS message to recipients.

    :param message: Message - The message to sent
    :param recipients: Union[tuple, list, set] - The list of recipients (fully-qualified phone numbers).
    :param test: bool - Determines if the message should be send in test mode. Warning: requires support by provider.
    :param provider_class: str - A class name of provider to use
    :raise: Exception
    :return: DispatchResult
    """
    try:
        result = get_sms_provider(provider_class).send(message, recipients, test)
        sms_send_success.send(__name__, message=message, recipients=recipients, result=result, test=test)
    except Exception as ex:
        sms_send_failure.send(__name__, message=message, recipients=recipients, exception=ex, test=test)
        raise ex
    else:
        return result
