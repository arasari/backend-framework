import logging
import re
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from telegram import Update

from arasari.core.utils import import_class
from arasari.messengers.bots import Bot, IncomingMessage, Message
from arasari.messengers.connect import connect_messenger

logger = logging.getLogger(__name__)

_handlers = None


def get_handlers():
    global _handlers
    if not isinstance(_handlers, list):
        _handlers = []
        handler_classes = getattr(settings, 'MESSENGERS_REQUEST_HANDLERS')
        for handler in handler_classes:
            _handlers.append(
                import_class(handler)()
            )
    return _handlers


def apply_handlers(bot: Bot, msg: IncomingMessage):
    for handler in get_handlers():
        if handler.accepts(msg, bot):
            handler.handle(msg, bot)


class BaseHandler:
    def accepts(self, msg: IncomingMessage, bot: Bot) -> bool:
        raise NotImplemented

    def handle(self, msg: IncomingMessage, bot: Bot):
        raise NotImplemented


class ConnectChat(BaseHandler):
    """ Listens for a message in format xxxx-xxxx or xxxxxxxx
    and connects related user to the messenger. """

    def accepts(self, msg: IncomingMessage, *args) -> bool:
        if not msg.text:
            return False

        cleaned = self._clean(msg.text)
        return len(cleaned) == 8 and re.match(r'[\d]{8}', cleaned) is not None

    def handle(self, msg: IncomingMessage, bot: Bot):
        try:
            connect_messenger(self._clean(msg.text), msg.messenger, msg.chat_id)
            bot.send_message(
                msg.chat_id,
                Message(str(_('The bot has been connected and ready to work.')))
            )
        except ValueError as ex:
            bot.send_message(
                msg.chat_id,
                Message(str(ex))
            )

    def _clean(self, text: str):
        return text.strip().replace('-', '')


class BotAddedToGroup(BaseHandler):
    def accepts(self, msg: IncomingMessage, bot: Bot):
        from arasari.messengers.models import Subscriber

        if msg.messenger != 'telegram':
            return False

        update: Update = msg.raw
        if len(update.message.new_chat_members) == 0:
            return False

        for member in update.message.new_chat_members:
            if not member['is_bot']: continue

            if member['username'] == bot.adapter.bot.username:
                return not Subscriber.objects.filter(messenger=msg.messenger, chat_id=update.message.chat.id).exists()

        return False

    def handle(self, msg: IncomingMessage, bot: Bot):
        from arasari.messengers.models import Subscriber
        update: Update = msg.raw
        try:
            Subscriber.objects.create(
                messenger=msg.messenger,
                chat_id=update.message.chat.id,
                chat_name=update.message.chat.title,
                added_by=update.message['from_user'].username,
                added_by_id=update.message['from_user'].id
            )
        except Exception as ex:
            logger.exception(ex)


class BotRemovedFromGroup(BaseHandler):
    def accepts(self, msg: IncomingMessage, bot: Bot):
        if msg.messenger != 'telegram':
            return False

        update: Update = msg.raw

        if not update.message.left_chat_member:
            return False

        if not update.message.left_chat_member['is_bot']:
            return False

        return update.message.left_chat_member['username'] == bot.adapter.bot.username

    def handle(self, msg: IncomingMessage, bot: Bot):
        from arasari.messengers.models import Subscriber
        update: Update = msg.raw
        Subscriber.objects.filter(messenger=msg.messenger, chat_id=update.message.chat.id).delete()
