from django.core.management import BaseCommand, CommandParser, CommandError

from arasari.core.urls import abs_reverse
from arasari.messengers.telegram import get_telegram_bot
from arasari.messengers.viber import get_viber_bot


class Command(BaseCommand):
    def add_arguments(self, parser: CommandParser):
        parser.add_argument('messenger')
        parser.add_argument('chat_id')
        parser.add_argument('text')
        parser.add_argument('--bot_name', '-b', default='default')

    def handle(self, messenger, chat_id, text, bot_name, *args, **options):
        bot = None
        if messenger == 'telegram':
            bot = get_telegram_bot(bot_name)

        if messenger == 'viber':
            bot = get_viber_bot(bot_name)

        if not bot:
            raise CommandError('Invalid bot type.')
        bot.send_message(chat_id, text)
