from django.core.management import BaseCommand

from arasari.core.urls import abs_reverse
from arasari.messengers.telegram import get_telegram_bot
from arasari.messengers.viber import get_viber_bot


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.install_telegram_webhook()
        self.install_viber_webhook()

    def install_viber_webhook(self):
        bot = get_viber_bot()
        url = abs_reverse('messenger_webhook', kwargs={'messenger': 'viber'})
        bot.set_webhook(url)

        self.stdout.write(f'[viber]: {url}')

    def install_telegram_webhook(self):
        bot = get_telegram_bot()
        url = abs_reverse('messenger_webhook', kwargs={'messenger': 'telegram'})
        bot.set_webhook(url)

        self.stdout.write(f'[telegram]: {url}')
