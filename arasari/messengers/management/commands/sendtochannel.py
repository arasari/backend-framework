from django.core.management import BaseCommand, CommandParser

from arasari.messengers.telegram import send_message


class Command(BaseCommand):
    def add_arguments(self, parser: CommandParser):
        parser.add_argument('channel_name')
        parser.add_argument('message')

    def handle(self, channel_name, message, *args, **options):
        send_message(channel_name, message)
