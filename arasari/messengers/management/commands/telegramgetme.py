from django.core.management import BaseCommand
from pprint import pprint

from arasari.messengers.telegram import get_telegram_bot


class Command(BaseCommand):
    """ Returns information of the Telegram bot. """

    def handle(self, *args, **options):
        bot = get_telegram_bot()
        info = bot.getMe()
        print(info)
