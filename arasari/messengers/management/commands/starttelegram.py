import logging
from django.core.management import BaseCommand
from telegram.ext import CommandHandler, Filters, MessageHandler, Updater

from arasari.messengers.telegram import get_telegram_bot, handle_incoming_message, start_command

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        bot = get_telegram_bot()
        updater = Updater(bot=bot)

        dispatcher = updater.dispatcher

        start_handler = CommandHandler('start', start_command)
        dispatcher.add_handler(start_handler)
        dispatcher.add_handler(MessageHandler(Filters.text, handle_incoming_message))

        updater.start_polling()
        updater.idle()
