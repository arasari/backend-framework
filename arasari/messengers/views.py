from rest_framework import status
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from arasari.messengers.models import LinkedChats
from arasari.messengers.notifications import TestNotification
from arasari.messengers.serializers import LinkedChatSerializer
from arasari.messengers.telegram import handle_webhook_request as handle_telegram_webhook_request
from arasari.messengers.viber import InvalidSignatureError, handle_webhook_request as handle_viber_webhook_request


class StatusView(RetrieveAPIView):
    serializer_class = LinkedChatSerializer
    queryset = LinkedChats.objects.all()

    def get_object(self):
        return self.request.user.linked_chats


class SendTestMessageView(APIView):
    def post(self, request, messenger):
        if messenger == 'telegram':
            self.send_telegram_message(request)

        if messenger == 'viber':
            self.send_viber_message(request)

        return Response({
            'message': 'sent'
        })

    def send_telegram_message(self, request):
        if request.user.linked_chats.telegram_connected:
            request.user.send_notification(TestNotification(), only=['telegram'])

    def send_viber_message(self, request):
        if request.user.linked_chats.viber_connected:
            request.user.send_notification(TestNotification(), only=['viber'])


class DisconnectView(APIView):
    def delete(self, request, messenger):
        request.user.linked_chats.disconnect_messenger(messenger)
        return Response({
            'message': 'disconnected'
        })


class WebhookEndpointView(APIView):
    """ This endpoint handles all incoming messages from various messenger agents. """

    permission_classes = (AllowAny,)

    def post(self, request, messenger):
        if messenger == 'telegram':
            handle_telegram_webhook_request(request)
            return Response(status=status.HTTP_200_OK)

        if messenger == 'viber':
            try:
                handle_viber_webhook_request(request)
                return Response(status=status.HTTP_200_OK)
            except InvalidSignatureError:
                return Response(status=status.HTTP_403_FORBIDDEN)

        return Response(status=status.HTTP_404_NOT_FOUND)
