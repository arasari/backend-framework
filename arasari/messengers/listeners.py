from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import LinkedChats

User = get_user_model()


@receiver(post_save, sender=User)
def on_attach_notification_settings(sender, instance, created, **kwargs):
    if created:
        LinkedChats.objects.create(user=instance)
