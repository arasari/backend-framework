from telegram import Bot as TelegramBot, Update

from arasari.messengers.bots import Bot, IncomingMessage, Message, TelegramAdapter
from arasari.messengers.handlers import apply_handlers
from arasari.messengers.registry import Registry, get_bot_settings
from arasari.notifications.channels import BaseChannel
from arasari.notifications.notification import DeliveryError

_registry = Registry()


def get_telegram_bot(name='default') -> Bot:
    """ Constructs and returns default Telegram bot instance. """

    global _registry

    if not _registry.has(name):
        bot_settings = get_bot_settings('TELEGRAM_BOTS', name)
        telegram_bot = TelegramBot(bot_settings.get('TOKEN'))
        bot = Bot(TelegramAdapter(telegram_bot))
        _registry.set(name, bot)
    return _registry.get(name)


def send_message(chat_id, message: Message, bot='default'):
    """ A shortcut to send text message. """

    get_telegram_bot(bot).send_message(chat_id, message)


class TelegramChannel(BaseChannel):
    """ Telegram channel for arasari.notifications. """

    name = 'telegram'

    def send(self, notification, user):
        if not hasattr(notification, 'to_chat_message'):
            raise TypeError(
                f'{notification.__class__} must implement "to_chat_message(self, user, builder) -> Message" method.'
            )

        message = notification.to_chat_message(user)
        if not message or not isinstance(message, Message):
            raise TypeError(f'{notification.__class__} must return Message class.')

        try:
            send_message(user.linked_chats.telegram_id, message)
        except Exception as ex:
            raise DeliveryError(ex)


def handle_webhook_request(request, bot_name='default'):
    """ Receives HTTP request and converts it into Telegram's Update object. """

    bot = get_telegram_bot(bot_name)
    update = Update.de_json(request.data, bot)
    message = IncomingMessage(update.message.text, update.message.chat_id, 'telegram', raw=update)
    apply_handlers(bot, message)
