from viberbot import Api, BotConfiguration
from viberbot.api.messages import TextMessage
from viberbot.api.viber_requests import ViberMessageRequest

from arasari.messengers.bots import Bot, IncomingMessage, Message, ViberAdapter
from arasari.messengers.handlers import apply_handlers
from arasari.messengers.registry import Registry, get_bot_settings
from arasari.notifications.channels import BaseChannel
from arasari.notifications.notification import DeliveryError

_registry = Registry()


def get_viber_bot(name='default') -> Bot:
    global _registry

    if not _registry.has(name):
        bot_settings = get_bot_settings('VIBER_BOTS', name)
        config = BotConfiguration(
            bot_settings.get('TOKEN'),
            bot_settings.get('NAME'),
            bot_settings.get('AVATAR'),
        )
        bot = Bot(ViberAdapter(Api(config)))
        _registry.set(name, bot)
    return _registry.get(name)


def send_message(chat_id, message: Message) -> list:
    """ Send a message via Viber. """

    return get_viber_bot().send_messages(chat_id, [
        TextMessage(text=message.text)
    ])


class ViberChannel(BaseChannel):
    """ Viber channel for arasari.notifications. """

    name = 'viber'

    def send(self, notification, user):
        if not hasattr(notification, 'to_chat_message'):
            raise TypeError(f'{notification.__class__} must implement "to_chat_message(self, user, builder) method."')

        message = notification.to_chat_message(user)
        if not message or not isinstance(message, Message):
            raise TypeError(f'{notification.__class__} must return Message class.')

        try:
            send_message(user.linked_chats.viber_id, message)
        except Exception as ex:
            raise DeliveryError(ex)


class InvalidSignatureError(ValueError): pass


def handle_webhook_request(request):
    viber = get_viber_bot()
    if not viber.adapter.verify_signature(request.body, request.META.get('HTTP_X_VIBER_CONTENT_SIGNATURE')):
        raise InvalidSignatureError()

    viber_request = viber.adapter.parse_request(request.body)
    bot = get_viber_bot()

    if isinstance(viber_request, ViberMessageRequest):
        message = IncomingMessage(viber_request.message.text, viber_request.sender.id, 'viber', raw=viber_request)
        apply_handlers(bot, message)
