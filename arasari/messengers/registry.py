from django.conf import settings


class Registry:
    def __init__(self):
        self._bots = {}

    def set(self, name, bot):
        if self.has(name):
            raise ValueError(f'Bot with name "{name}" is already registered.')
        self._bots[name] = bot

    def get(self, name):
        if not self.has(name):
            raise ValueError(f'Bot with name "{name}" is not registered.')
        return self._bots[name]

    def has(self, name):
        return name in self._bots

    def delete(self, name):
        if not self.has(name):
            raise ValueError(f'Bot with name "{name}" is not registered.')

        del self._bots[name]


class ConfigurationError(ValueError): pass


def get_bot_settings(settings_key, bot_key):
    bot_settings = getattr(settings, settings_key, None)
    if not bot_settings:
        raise ConfigurationError(
            f'Bot is not configured in settings. Please, setup {settings_key} option.'
        )

    if bot_key not in bot_settings:
        raise ConfigurationError(
            f'No bot with name "{bot_key}" is configured in {settings_key}.'
        )

    if 'TOKEN' not in bot_settings.get(bot_key):
        raise ConfigurationError(f'Bot "{bot_key}" does not have TOKEN configured in {settings_key}.')
    return bot_settings.get(bot_key)
