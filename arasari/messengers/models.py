import random
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from arasari.messengers.bots import Message
from arasari.messengers.connect import disconnect
from arasari.messengers.telegram import send_message as send_telegram_message
from arasari.messengers.viber import send_message as send_viber_message

DEFAULT_TTL = 60 * 15


def generate_code():
    return random.randint(1000_0000, 9999_9999)


class LinkedChats(models.Model):
    """ Keeps information between user and its connected chats. """

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='linked_chats')
    connect_code = models.CharField(
        _('messenger connect code'), max_length=12, unique=True, db_index=True,
        default=generate_code
    )
    telegram_id = models.CharField(_('telegram user id'), max_length=64, null=True, blank=True)
    viber_id = models.CharField(_('viber user id'), max_length=64, null=True, blank=True)

    class Meta:
        verbose_name = _('Linked chat')
        verbose_name_plural = _('Linked chats')

    @property
    def telegram_connected(self):
        """ Tests if Telegram bot connected. """
        return self.telegram_id is not None

    def set_telegram_id(self, chat_id):
        """ Set Telegram user id and save this model. """

        self.telegram_id = chat_id
        self.save()

    @property
    def viber_connected(self):
        """ Tests if Viber bot connected. """

        return self.viber_id is not None

    def set_viber_id(self, chat_id):
        """ Set Viber user id and save this model. """

        self.viber_id = chat_id
        self.save()

    def disconnect_messenger(self, messenger):
        """ Disconnect messenger from user. """

        return disconnect(self.user, messenger)

    def send_telegram_message(self, message: Message):
        """ Sends a message via Telegram. """

        if self.telegram_connected:
            send_telegram_message(self.telegram_id, message)

    def send_viber_message(self, message: Message):
        """ Sends a message via Viber. """

        if self.viber_connected:
            send_viber_message(self.telegram_id, message)


class Subscriber(models.Model):
    messenger = models.CharField(max_length=32)
    chat_id = models.CharField(max_length=128, db_index=True)
    chat_name = models.CharField(max_length=128)
    added_by = models.CharField(max_length=128)
    added_by_id = models.CharField(max_length=128)
    added_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Subscriber')
        verbose_name_plural = _('Subscribers')
        ordering = ('-added_at',)
        unique_together = (
            ('messenger', 'chat_id')
        )
