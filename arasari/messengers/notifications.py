from django.utils.translation import ugettext_lazy as _

from arasari.messengers.bots import Message
from arasari.notifications.notification import BaseNotification


class TestNotification(BaseNotification):
    def via(self, user):
        return ['telegram', 'viber']

    def to_chat_message(self, user):
        builder = Message.TextBuilder()
        builder.line(str(_('Hi, %(user_name)s') % {'user_name': user.first_name}))
        builder.line(str(_('This is the test message.')))

        return Message(builder.build())
