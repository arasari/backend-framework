from django.test import TestCase, override_settings

from arasari.messengers.bots import BaseAdapter, Bot
from arasari.messengers.connect import connect_messenger
from arasari.messengers.registry import ConfigurationError, Registry, get_bot_settings
from arasari.users.test_utils import ManagesUsers


class ConnectMessengerTestCase(TestCase, ManagesUsers):
    def test_connects_with_valid_code(self):
        user = self.create_user()
        connect_messenger(user.linked_chats.connect_code, 'telegram', 'chat_id')

        user.refresh_from_db()
        self.assertTrue(user.linked_chats.telegram_connected)

    def test_fails_to_connect_with_invalid_code(self):
        with self.assertRaises(ValueError):
            connect_messenger('invalid code', 'telegram', 'chat_id')


class RegistryTestCase(TestCase):
    def test_sets_bot(self):
        bot = Bot(BaseAdapter())
        registry = Registry()
        registry.set('name', bot)
        self.assertTrue(registry.has('name'))

    def test_gets_bot(self):
        bot = Bot(BaseAdapter())
        registry = Registry()
        registry.set('name', bot)
        self.assertEquals(registry.get('name'), bot)

    def test_has_bot(self):
        bot = Bot(BaseAdapter())
        registry = Registry()
        registry.set('name', bot)
        self.assertTrue(registry.has('name'))

    def test_delete_bot(self):
        bot = Bot(BaseAdapter())
        registry = Registry()
        registry.set('name', bot)
        registry.delete('name')

        self.assertFalse(registry.has('name'))


class GetBotSettingsTestCase(TestCase):
    def test_returns_settings(self):
        bot_settings = {'default': {'TOKEN': 'sometoken'}}
        with override_settings(TELEGRAM_BOTS=bot_settings):
            settings = get_bot_settings('TELEGRAM_BOTS', 'default')
            self.assertEquals(settings, bot_settings.get('default'))

    def test_fails_to_construct_if_no_setting(self):
        with override_settings(TELEGRAM_BOTS=None):
            with self.assertRaises(ConfigurationError):
                get_bot_settings('TELEGRAM_BOTS', 'default')

    def test_fails_to_construct_if_bot_not_defined_in_settings(self):
        with override_settings(TELEGRAM_BOTS={}):
            with self.assertRaises(ConfigurationError):
                get_bot_settings('TELEGRAM_BOTS', 'default')

        with override_settings(TELEGRAM_BOTS={'default': {'TOKEN': 'sometoken'}}):
            with self.assertRaises(ConfigurationError):
                get_bot_settings('TELEGRAM_BOTS', 'custom')
