from django.test import TestCase
from telegram import Update
from unittest import mock

from arasari.messengers.bots import BaseAdapter, Bot, IncomingMessage, TelegramAdapter
from arasari.messengers.handlers import ConnectChat, BotAddedToGroup, BotRemovedFromGroup
from arasari.messengers.models import Subscriber
from arasari.users.test_utils import ManagesUsers
from telegram import Bot as TelegramBot


class ConnectChatTestCase(TestCase, ManagesUsers):

    def test_clean(self):
        handler = ConnectChat()
        self.assertEqual('11112222', handler._clean('1111-2222\n'))
        self.assertEqual('11112222', handler._clean('11112222\n'))
        self.assertEqual('11112222', handler._clean('   1111-2222\n'))
        self.assertEqual('11112222', handler._clean('\n\n1111-2222\n'))

    def test_accepts(self):
        handler = ConnectChat()
        self.assertTrue(handler.accepts(
            IncomingMessage('1111-2222', 'telegram', 'chat_id')
        ))
        self.assertTrue(handler.accepts(
            IncomingMessage('\n1111-2222\n', 'telegram', 'chat_id')
        ))
        self.assertFalse(handler.accepts(
            IncomingMessage('11111-2222', 'telegram', 'chat_id')
        ))
        self.assertFalse(handler.accepts(
            IncomingMessage('other text', 'telegram', 'chat_id')
        ))

    def test_handle_ok(self):
        user = self.create_user()
        handler = ConnectChat()
        msg1 = IncomingMessage(str(user.linked_chats.connect_code), 'telegram', 'chat_id')

        with mock.patch('arasari.messengers.bots.Bot.send_message') as send_message_mock:
            bot = Bot(BaseAdapter())
            handler.handle(msg1, bot)
            send_message_mock.assert_called()

    def test_handle_error(self):
        handler = ConnectChat()
        msg1 = IncomingMessage('invalid code', 'telegram', 'chat_id')

        with mock.patch('arasari.messengers.bots.Bot.send_message'):
            with mock.patch('arasari.messengers.connect.connect_messenger') as connect_messenger_mock:
                handler.handle(msg1, Bot(BaseAdapter()))
            connect_messenger_mock.assert_not_called()


class BotAddedToGroupTestCase(TestCase):
    @mock.patch('telegram.Bot._validate_token', lambda self, token: token)
    @mock.patch('telegram.Bot.get_me', lambda self: 'CarParts_devbot')
    @mock.patch('telegram.Bot.username', 'CarParts_devbot')
    def test_accepts_message(self):
        handler = BotAddedToGroup()
        msg_json = {
            "update_id": 929467809,
            "message": {
                "message_id": 12,
                "date": 1540406075,
                "chat": {
                    "id": -233876284,
                    "type": "group",
                    "title": "CARPARTS - TEST"
                },
                "entities": [], "caption_entities": [], "photo": [],
                "new_chat_members": [
                    {"id": 701603859, "first_name": "carpartsby_devbot", "is_bot": True, "username": "CarParts_devbot"}
                ],
                "left_chat_member": {
                },
                "new_chat_photo": [], "delete_chat_photo": False,
                "group_chat_created": False,
                "supergroup_chat_created": False,
                "channel_chat_created": False,
                "from": {
                    "id": 110863971, "first_name": "Alex", "is_bot": False, "last_name": "Oleshkevich",
                    "username": "alexoleshkevich", "language_code": "en-US"
                }
            }
        }

        bot = Bot(TelegramAdapter(TelegramBot('token')))
        update = Update.de_json(msg_json, bot)
        msg = IncomingMessage('', '', 'telegram', update)
        self.assertTrue(handler.accepts(msg, bot))

    @mock.patch('telegram.Bot._validate_token', lambda self, token: token)
    @mock.patch('telegram.Bot.get_me', lambda self: 'CarParts_devbot')
    @mock.patch('telegram.Bot.username', 'CarParts_devbot')
    def test_handles_message(self):
        handler = BotAddedToGroup()
        msg_json = {
            "update_id": 929467809,
            "message": {
                "message_id": 12,
                "date": 1540406075,
                "chat": {
                    "id": -233876284,
                    "type": "group",
                    "title": "CARPARTS - TEST"
                },
                "entities": [], "caption_entities": [], "photo": [],
                "new_chat_members": [
                    {"id": 701603859, "first_name": "carpartsby_devbot", "is_bot": True, "username": "CarParts_devbot"}
                ],
                "left_chat_member": {
                },
                "new_chat_photo": [], "delete_chat_photo": False,
                "group_chat_created": False,
                "supergroup_chat_created": False,
                "channel_chat_created": False,
                "from": {
                    "id": 110863971, "first_name": "Alex", "is_bot": False, "last_name": "Oleshkevich",
                    "username": "alexoleshkevich", "language_code": "en-US"
                }
            }
        }

        bot = Bot(TelegramAdapter(TelegramBot('token')))
        update = Update.de_json(msg_json, bot)
        msg = IncomingMessage('', '', 'telegram', update)
        handler.handle(msg, bot)
        self.assertEquals(1, Subscriber.objects.filter(
            messenger='telegram', chat_id=msg_json['message']['chat']['id']
        ).count())


class BotRemoveFromGroupTestCase(TestCase):
    @mock.patch('telegram.Bot._validate_token', lambda self, token: token)
    @mock.patch('telegram.Bot.get_me', lambda self: 'CarParts_devbot')
    @mock.patch('telegram.Bot.username', 'CarParts_devbot')
    def test_accepts_message(self):
        handler = BotRemovedFromGroup()
        msg_json = {
            "update_id": 929467809,
            "message": {
                "message_id": 12,
                "date": 1540406075,
                "chat": {
                    "id": -233876284, "type": "group", "title": "CARPARTS - TEST"
                },
                "entities": [], "caption_entities": [], "photo": [],
                "new_chat_members": [],
                "left_chat_member": {
                    "id": 701603859, "first_name": "carpartsby_devbot", "is_bot": True, "username": "CarParts_devbot"
                },
                "new_chat_photo": [], "delete_chat_photo": False,
                "group_chat_created": False,
                "supergroup_chat_created": False,
                "channel_chat_created": False,
                "from": {
                    "id": 110863971, "first_name": "Alex", "is_bot": False, "last_name": "Oleshkevich",
                    "username": "alexoleshkevich", "language_code": "en-US"
                }
            }
        }

        bot = Bot(TelegramAdapter(TelegramBot('token')))
        update = Update.de_json(msg_json, bot)
        msg = IncomingMessage('', '', 'telegram', update)
        self.assertTrue(handler.accepts(msg, bot))

    @mock.patch('telegram.Bot._validate_token', lambda self, token: token)
    @mock.patch('telegram.Bot.get_me', lambda self: 'CarParts_devbot')
    @mock.patch('telegram.Bot.username', 'CarParts_devbot')
    def test_handles_message(self):
        handler = BotRemovedFromGroup()
        msg_json = {
            "update_id": 929467809,
            "message": {
                "message_id": 12,
                "date": 1540406075,
                "chat": {
                    "id": -233876284, "type": "group", "title": "CARPARTS - TEST"
                },
                "entities": [], "caption_entities": [], "photo": [],
                "new_chat_members": [
                ],
                "left_chat_member": {
                    "id": 701603859, "first_name": "carpartsby_devbot", "is_bot": True, "username": "CarParts_devbot"
                },
                "new_chat_photo": [], "delete_chat_photo": False,
                "group_chat_created": False,
                "supergroup_chat_created": False,
                "channel_chat_created": False,
                "from": {
                    "id": 110863971, "first_name": "Alex", "is_bot": False, "last_name": "Oleshkevich",
                    "username": "alexoleshkevich", "language_code": "en-US"
                }
            }
        }

        Subscriber.objects.create(messenger='telegram', chat_id=msg_json['message']['chat']['id'])

        bot = Bot(TelegramAdapter(TelegramBot('token')))
        update = Update.de_json(msg_json, bot)
        msg = IncomingMessage('', '', 'telegram', update)
        handler.handle(msg, bot)
        self.assertEquals(0, Subscriber.objects.filter(
            messenger='telegram', chat_id=msg_json['message']['chat']['id']
        ).count())
