from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.response import Response
from unittest import mock

from arasari.users.test_utils import ManagesUsers


class StatusViewTestCase(TestCase, ManagesUsers):
    def setUp(self):
        self.user = self.create_user()
        self.client.force_login(self.user)

    def test_returns_status_with_code(self):
        url = reverse('messengers_status')

        response: Response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.assertIn('code', response.data)
        self.assertIn('telegram', response.data)
        self.assertIn('viber', response.data)

        self.assertEquals(int(response.data['code']), int(self.user.linked_chats.connect_code))


class SendTestMessageViewTestCase(TestCase, ManagesUsers):
    def setUp(self):
        self.user = self.create_user()
        self.client.force_login(self.user)

    @mock.patch('arasari.notifications.models.Notifiable.send_notification')
    def test_sends_test_telegram_message(self, silent_send_notification_mock):
        url = reverse('messengers_check', kwargs={'messenger': 'telegram'})

        self.user.linked_chats.telegram_id = '123123'
        self.user.linked_chats.save()

        response: Response = self.client.post(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        silent_send_notification_mock.assert_called()

    @mock.patch('arasari.notifications.models.Notifiable.send_notification')
    def test_donot_sends_test_telegram_message(self, silent_send_notification_mock):
        url = reverse('messengers_check', kwargs={'messenger': 'telegram'})

        response: Response = self.client.post(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        silent_send_notification_mock.assert_not_called()

    @mock.patch('arasari.notifications.models.Notifiable.send_notification')
    def test_sends_test_viber_message(self, silent_send_notification_mock):
        url = reverse('messengers_check', kwargs={'messenger': 'viber'})

        self.user.linked_chats.viber_id = '123123'
        self.user.linked_chats.save()

        response: Response = self.client.post(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        silent_send_notification_mock.assert_called()

    @mock.patch('arasari.notifications.models.Notifiable.send_notification')
    def test_donot_sends_test_viber_message(self, silent_send_notification_mock):
        url = reverse('messengers_check', kwargs={'messenger': 'viber'})

        response: Response = self.client.post(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        silent_send_notification_mock.assert_not_called()


class DisconnectTestCase(TestCase, ManagesUsers):
    def setUp(self):
        self.user = self.create_user()
        self.client.force_login(self.user)
        self.user.linked_chats.telegram_id = '123'
        self.user.linked_chats.viber_id = '123'
        self.user.linked_chats.save()

    def test_disconnects_telegram(self):
        url = reverse('messengers_disconnect', kwargs={'messenger': 'telegram'})
        response: Response = self.client.delete(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.user.refresh_from_db()
        self.assertIsNone(self.user.linked_chats.telegram_id)

    def test_disconnects_viber(self):
        url = reverse('messengers_disconnect', kwargs={'messenger': 'viber'})
        response: Response = self.client.delete(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.user.refresh_from_db()
        self.assertIsNone(self.user.linked_chats.viber_id)


class WebhookEndpointViewTestCase(TestCase):
    @mock.patch('arasari.messengers.views.handle_telegram_webhook_request')
    def test_telegram(self, handle_telegram_webhook_request_mock):
        url = reverse('messenger_webhook', kwargs={'messenger': 'telegram'})
        response = self.client.post(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        handle_telegram_webhook_request_mock.assert_called()

    @mock.patch('arasari.messengers.views.handle_viber_webhook_request')
    def test_viber(self, handle_viber_webhook_request_mock):
        url = reverse('messenger_webhook', kwargs={'messenger': 'viber'})
        response = self.client.post(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        handle_viber_webhook_request_mock.assert_called()

    def test_error(self):
        url = reverse('messenger_webhook', kwargs={'messenger': 'unknown'})
        response = self.client.post(url)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)
