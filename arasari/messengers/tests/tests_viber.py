from django.test import TestCase, override_settings
from unittest import mock
from unittest.mock import MagicMock

from arasari.messengers.bots import Bot, Message, ViberAdapter
from arasari.messengers.registry import ConfigurationError, Registry
from arasari.messengers.viber import get_viber_bot, ViberChannel
from arasari.notifications.notification import BaseNotification
from arasari.users.test_utils import ManagesUsers


class GetViberBotTestCase(TestCase):
    def test_constructs_bot(self):
        telegram_settings = {'default': {'TOKEN': 'sometoken'}}
        with override_settings(VIBER_BOTS=telegram_settings):
            bot = get_viber_bot('default')
            self.assertIsInstance(bot, Bot)
            self.assertIsInstance(bot.adapter, ViberAdapter)

    @mock.patch('arasari.messengers.viber._registry', Registry())
    def test_fails_to_construct_if_no_setting(self):
        with override_settings(VIBER_BOTS=None):
            with self.assertRaises(ConfigurationError):
                get_viber_bot('default')

    @mock.patch('arasari.messengers.viber._registry', Registry())
    def test_fails_to_construct_if_bot_not_defined_in_settings(self):
        with override_settings(VIBER_BOTS={}):
            with self.assertRaises(ConfigurationError):
                get_viber_bot('default')


class SendMessageTestCase(TestCase):
    @mock.patch('arasari.messengers.bots.Bot.send_message')
    def test_send_message(self, send_message_mock: MagicMock):
        viber_settings = {'default': {'TOKEN': 'sometoken'}}
        with override_settings(VIBER_BOTS=viber_settings):
            bot = get_viber_bot()
            msg = Message('message')
            bot.send_message('chat_id', msg)
        send_message_mock.assert_called_once_with('chat_id', msg)


class ViberChannelTestCase(TestCase, ManagesUsers):
    def test_send_ok(self):
        class Notification(BaseNotification):
            def to_chat_message(self, *args, **kwargs):
                return Message('text')

        user = self.create_user()
        channel = ViberChannel()

        with mock.patch('arasari.messengers.viber.send_message') as send_message_mock:
            channel.send(Notification(), user)
        send_message_mock.assert_called_once()

    def test_send_errors_if_has_no_to_chat_message_method(self):
        class Notification(BaseNotification):
            pass

        user = self.create_user()
        channel = ViberChannel()

        with self.assertRaises(TypeError):
            channel.send(Notification(), user)

    def test_send_errors_if_doesnot_returns_message(self):
        class Notification(BaseNotification):
            def to_chat_message(self, *args, **kwargs):
                pass

        user = self.create_user()
        channel = ViberChannel()

        with self.assertRaises(TypeError):
            channel.send(Notification(), user)
