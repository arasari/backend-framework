from django.utils.translation import ugettext as _


def connect_messenger(code: str, messenger: str, chat_id: str):
    """ Connects messenger and user. """

    from arasari.messengers.models import LinkedChats

    try:
        linked_chat = LinkedChats.objects.filter(connect_code=code).get()
    except LinkedChats.DoesNotExist:
        raise ValueError(_('This connect code is invalid.'))

    if messenger == 'telegram':
        linked_chat.set_telegram_id(chat_id)

    if messenger == 'viber':
        linked_chat.set_viber_id(chat_id)


def disconnect(user, messenger):
    """ Disconnect messenger from user. """

    if messenger == 'telegram':
        user.linked_chats.telegram_id = None

    if messenger == 'viber':
        user.linked_chats.viber_id = None

    user.linked_chats.save()
