from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.utils.translation import ugettext_lazy as _

from arasari.messengers.models import LinkedChats, Subscriber


@admin.register(LinkedChats)
class LinkedChatsAdmin(ModelAdmin):
    list_display = (
        'user', 'connect_code', 'viber_connected', 'telegram_connected',
    )
    search_fields = ('user__email', 'user__last_name',)

    def viber_connected(self, obj): return obj.viber_connected

    viber_connected.boolean = True
    viber_connected.description = _('viber connected')

    def telegram_connected(self, obj): return obj.telegram_connected

    telegram_connected.boolean = True
    viber_connected.description = _('telegram connected')


@admin.register(Subscriber)
class SubscriberAdmin(ModelAdmin):
    list_display = ('messenger', 'chat_name', 'added_by', 'added_at',)
    list_filter = ('messenger', 'added_at', 'added_at')
    search_fields = ('chat_name', 'added_by',)
    readonly_fields = ('chat_id', 'added_by_id', 'messenger', 'chat_name', 'added_by', 'added_at',)
