from rest_framework import serializers

from arasari.messengers.models import LinkedChats


class LinkedChatSerializer(serializers.ModelSerializer):
    telegram = serializers.SerializerMethodField()
    viber = serializers.SerializerMethodField()
    code = serializers.CharField(source='connect_code')

    class Meta:
        model = LinkedChats
        fields = ('telegram', 'viber', 'code')

    def get_telegram(self, obj):
        return obj.telegram_connected

    def get_viber(self, obj):
        return obj.viber_connected
