from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DefaultAppConfig(AppConfig):
    name = 'arasari.messengers'
    label = 'arasari_messengers'
    verbose_name = _('Messengers')

    def ready(self):
        # noinspection PyUnresolvedReferences
        import arasari.messengers.listeners
