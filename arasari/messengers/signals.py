from django.dispatch import Signal

chat_message_send_success = Signal(providing_args=['messenger', 'chat_id', 'message'])
chat_message_send_failure = Signal(providing_args=['messenger', 'chat_id', 'message', 'exception'])
