import logging
from typing import Union

from telegram import Bot as TelegramBot
from viberbot import Api
from viberbot.api.messages import TextMessage

from arasari.messengers.signals import chat_message_send_success, chat_message_send_failure

logger = logging.getLogger(__name__)


class Message:
    text = None

    def __init__(self, text=None):
        self.text = text

    def __repr__(self):
        return f'{self.__class__}: {self.text}'

    def __str__(self):
        return self.text

    class TextBuilder:
        def __init__(self):
            self._lines = []
            self._images = []

        def line(self, text=''):
            self._lines.append(text)

        def image(self, url):
            self._images.append(url)

        def build(self):
            return '\n'.join(map(str, filter(lambda x: x is not None, self._lines)))

        def as_message(self):
            return Message(self.build())


class IncomingMessage:
    """ Represents unified incoming message. """

    def __init__(self, text, chat_id, messenger, raw=None):
        self.text = text
        self.chat_id = chat_id
        self.messenger = messenger
        self.raw = raw


class Bot:
    adapter = None

    def __init__(self, adapter: 'BaseAdapter'):
        self.adapter = adapter

    def send_message(self, chat_id: str, message: Union[Message, str]):
        if isinstance(message, str):
            message = Message(message)

        try:
            self.adapter.send_message(chat_id, message)
        except Exception as ex:
            chat_message_send_failure.send(
                self, messenger=self.adapter.name, chat_id=chat_id, message=message, exception=ex
            )

            clean_text = message.text.replace(r'\n', '\\n')
            logger.error(f'Failed to send message "{clean_text}" to "{chat_id}" via {self.adapter.name}.')
            logger.exception(ex)
            return False
        else:
            chat_message_send_success.send(self, messenger=self.adapter.name, chat_id=chat_id, message=message)
            return True

    def __getattr__(self, item):
        return getattr(self.adapter, item)


class BaseAdapter:
    name = '__base__'

    def send_message(self, chat_id: str, message: Message):
        raise NotImplemented


class ViberAdapter(BaseAdapter):
    """ Viber connection provider. """

    name = 'viber'

    def __init__(self, bot: Api):
        self.bot: Api = bot

    def send_message(self, chat_id: str, message: Message):
        return self.bot.send_messages(chat_id, [
            TextMessage(text=message.text)
        ])

    def __getattr__(self, item):
        return getattr(self.bot, item)


class TelegramAdapter(BaseAdapter):
    """ Telegram connection provider. """

    name = 'telegram'

    def __init__(self, bot: TelegramBot):
        self.bot: TelegramBot = bot

    def send_message(self, chat_id: str, message: Message):
        return self.bot.send_message(chat_id, message.text)

    def __getattr__(self, item):
        return getattr(self.bot, item)
