from django.conf import settings
from django.urls import path

from . import views

secret = getattr(settings, 'MESSENGERS_WEBHOOK_SECRET', '')

urlpatterns = [
    path('messengers/check/<str:messenger>/', views.SendTestMessageView.as_view(), name='messengers_check'),
    path('messengers/status/', views.StatusView.as_view(), name='messengers_status'),
    path('messengers/disconnect/<str:messenger>/', views.DisconnectView.as_view(), name='messengers_disconnect'),
    path(
        f'messengers/___webhooks__{secret}/<str:messenger>/',
        views.WebhookEndpointView.as_view(), name='messenger_webhook'
    ),
]
