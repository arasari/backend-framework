from django.urls import include, path

from . import views

urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('auth/me/', views.GetCurrentUserView.as_view(), name='check-auth'),
    path('auth/permissions/', views.PermissionsView.as_view(), name='permissions'),
    path('password/forgot/', views.ForgotPasswordView.as_view(), name='forgot-password'),
    path('password/reset/', views.ResetPasswordView.as_view(), name='reset-password'),
    path('password/change/', views.ChangePasswordView.as_view(), name='change-password'),
    # path('register/', views.DefaultRegisterView.as_view(), name='users_register'),

    path('security/', include([
        path('email/confirm/<str:code>/', views.VerifyEmailView.as_view(), name='security_confirm_email'),
        path('email/resend/', views.ResendEmailConfirmationView.as_view(), name='security_resend_email'),
    ]))
]
