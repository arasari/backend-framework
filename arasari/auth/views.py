import uuid
from django.contrib.auth import get_user_model
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.views import View
from rest_framework import status
from rest_framework.generics import GenericAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from arasari.users.views import get_user_serializer
from .authentication import issue_token
from .mails import ForgotPasswordMail
from .models import PasswordReset
from .notifications import ConfirmRegistration
from .serializers import (ChangePasswordSerializer, DefaultUserSerializer, ForgotPasswordSerializer, LoginSerializer,
                          RegisterSerializer, ResetPasswordSerializer)
from .signals import user_registered

User = get_user_model()


class LoginView(APIView):
    """ Authenticates user. Returns API token. """

    serializer_class = LoginSerializer
    user_serializer = DefaultUserSerializer
    permission_classes = (AllowAny,)
    authentication_classes = []

    def post(self, request: Request) -> Response:
        serializer = self.serializer_class(
            data=self.request.data,
            context={
                'request': request
            }
        )
        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data['user']
        token = issue_token(user)

        return self.get_response(token, user)

    def get_response(self, token: str, user: User) -> Response:
        """ Returns authentication response. """
        user_data = self.user_serializer(user).data
        return Response({
            'token': token,
            'user': user_data
        })


class LogoutView(APIView):
    """ Logs out the user.
    Requires "Authorization: Bearer <token-string>" header.
    """
    permission_classes = (AllowAny,)

    def post(self, request):
        # if no _auth is not in request then probably other auth backend was used (eg. Session)
        if request._auth:
            request._auth.delete()

        return Response({
            'detail': 'Logged out.'
        })


class GetCurrentUserView(RetrieveAPIView, UpdateAPIView, GenericAPIView):
    """ Returns current user.
    May be used to test authentication from UI or tests."""

    queryset = User.objects.all()
    serializer_class = get_user_serializer()

    def get_object(self):
        return self.request.user


class ForgotPasswordView(APIView):
    """  Renders password reset request page. """
    permission_classes = (AllowAny,)
    authentication_classes = []

    def post(self, request):
        serializer = ForgotPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = User.objects.filter(email=request.data.get('email')).get()
        PasswordReset.objects.filter(user=user).delete()

        reset_code = PasswordReset.objects.create(token=uuid.uuid4(), user=user)
        reset_code.save()
        user.send_email_message(ForgotPasswordMail(user, reset_code))
        return Response({
            'message': 'ok'
        })


class PermissionsView(APIView):
    """ Returns user permissions. """
    permission_classes = (AllowAny,)

    def get(self, request):
        permissions = []
        if request.user.is_authenticated:
            permissions.append('authenticated')
        else:
            permissions.append('unauthenticated')
        return Response(permissions)


class ResetPasswordView(APIView):
    """ Renders "set new password" form. """
    permission_classes = (AllowAny,)
    authentication_classes = []

    def post(self, request):
        serializer = ResetPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = User.objects.filter(email=request.data.get('email')).get()
        PasswordReset.objects.filter(user=user).delete()
        user.set_password(request.data.get('password'))
        user.save()

        return Response({
            'message': 'ok'
        })


class ChangePasswordView(APIView):
    """ Change password form. Displayed in profile settings. """

    def post(self, request):
        serializer = ChangePasswordSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        request.user.set_password(request.data.get('password'))
        request.user.save()
        return Response({
            'message': 'changed',
        })


class RegistersUsers:
    serializer_class = RegisterSerializer

    def get_queryset(self):
        return User.objects.all()

    def post(self, request):
        serializer: RegisterSerializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = self.create_user(serializer.validated_data)

        user_registered.send(self, user=user, request=request)
        self.on_user_registered(user)

        return self.get_response(user)

    def create_user(self, validated_data):
        # get only a set of fields from request required to create a user
        # we do this to avoid getting unwanted data send by user
        fields = self.get_request_field_names()

        user_data = {k: v for k, v in validated_data.items() if k in fields}

        user = User(**user_data)
        user.set_password(validated_data.get('password'))
        user.date_joined = timezone.now()
        user.is_active = True
        user.save()
        return user

    def get_request_field_names(self):
        return ('first_name', 'last_name', 'email')

    def on_user_registered(self, user):
        pass

    def get_response(self, user):
        return Response({
            'message': 'ok',
            'user_id': user.id,
            'user_name': str(user),
        })


class RequiresEmailConfirmation:
    def on_user_registered(self, user):
        super().on_user_registered(user)

        user.confirmation_code = uuid.uuid4()
        user.save()

        user.send_notification(ConfirmRegistration())


class BaseRegisterView(RegistersUsers, GenericAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = []


class DefaultRegisterView(RequiresEmailConfirmation, BaseRegisterView): pass


class VerifyEmailView(View):

    def get(self, request, code):
        try:
            user = User.objects.filter(confirmation_code=code).get()
        except User.DoesNotExist:
            raise Http404('User was not found.')
        else:
            user.confirm_email()
            return HttpResponseRedirect(reverse('public_login'))


class ResendEmailConfirmationView(APIView):

    def post(self, request):
        user = request.user
        if user.is_confirmed:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)

        user.confirmation_code = uuid.uuid4()
        user.save()
        user.send_notification(ConfirmRegistration(), only=['mail'])
        return Response({
            'message': 'resent',
        })
