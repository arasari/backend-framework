import uuid
from django.contrib.auth import get_user_model
from django.core import mail
from django.http import HttpRequest
from django.test import TestCase
from rest_framework import status
from rest_framework.exceptions import AuthenticationFailed, ValidationError
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from arasari.core.urls import abs_reverse
from arasari.users.test_utils import ManagesUsers
from .authentication import (TokenAuthentication, authenticate, fetch_token, get_token_hash, issue_token)
from .models import ApiToken, PasswordReset
from .serializers import LoginSerializer
from .signals import user_interactive_login, user_login_error

User = get_user_model()


class AuthenticationTestCase(TestCase, ManagesUsers):

    def setUp(self):
        self.auth_interactive_login_signal_sent = False
        self.auth_error_signal_sent = False

    def test_issue_token(self):
        user = self.create_user()
        token = issue_token(user)
        self.assertIsNotNone(token)

    def test_fetch_token(self):
        user = self.create_user()
        token = issue_token(user)

        token_obj = fetch_token(token)
        self.assertIsInstance(token_obj, ApiToken)
        self.assertEqual(token_obj.token, get_token_hash(token))

    def test_get_token_hash(self):
        token = 'token'
        hashed = get_token_hash(token)
        self.assertIsInstance(hashed, str)

    def test_authenticate(self):
        request = Request(HttpRequest())
        email = self.get_faker().email()
        user = self.create_user(email=email, username=email)
        authenticated = authenticate(request, email, 'password')
        self.assertEqual(user.id, authenticated.id)

    def test_authenticate_fails_if_password_invalid(self):
        request = Request(HttpRequest())
        user = self.create_user()
        with self.assertRaises(AuthenticationFailed) as context:
            authenticate(request, user.email, 'invalid')
        self.assertEqual(context.exception.detail.code, 'invalid_credentials')

    def test_authenticate_fails_if_user_not_active(self):
        request = Request(HttpRequest())
        user = self.create_user(is_active=False)
        with self.assertRaises(AuthenticationFailed) as context:
            authenticate(request, user.email, 'password')
        self.assertEqual(context.exception.detail.code, 'invalid_credentials')

    def test_authenticate_sends_success_signal(self):
        def signal_handler(*args, **kwargs):
            self.auth_interactive_login_signal_sent = True

        user_interactive_login.connect(signal_handler)

        request = Request(HttpRequest())
        user = self.create_user()
        authenticate(request, user.email, 'password')
        user_interactive_login.disconnect(signal_handler)
        self.assertTrue(self.auth_interactive_login_signal_sent)

    def test_authenticate_sends_error_signal(self):
        def signal_handler(*args, **kwargs):
            self.auth_error_signal_sent = True

        user_login_error.connect(signal_handler)

        request = Request(HttpRequest())
        user = self.create_user()
        with self.assertRaises(AuthenticationFailed):
            authenticate(request, user.email, 'invalid')
        user_login_error.disconnect(signal_handler)
        self.assertTrue(self.auth_error_signal_sent)


class TokenAuthenticationTestCase(TestCase, ManagesUsers):
    def test_authenticate_credentials(self):
        user = self.create_user()
        token = issue_token(user)

        authenticator = TokenAuthentication()
        user, fetched_token = authenticator.authenticate_credentials(token)
        self.assertIsInstance(user, User)
        self.assertIsInstance(fetched_token, ApiToken)
        self.assertEqual(fetched_token.token, get_token_hash(token))

    def test_authenticate_credentials_fails_when_not_found(self):
        authenticator = TokenAuthentication()
        with self.assertRaises(AuthenticationFailed) as context:
            authenticator.authenticate_credentials('invalid token')
        self.assertEqual(context.exception.detail.code, 'invalid_token')

    def test_authenticate_credentials_fails_for_nonactive_user(self):
        user = self.create_user(is_active=False)
        token = issue_token(user)

        authenticator = TokenAuthentication()
        with self.assertRaises(AuthenticationFailed) as context:
            authenticator.authenticate_credentials(token)
        self.assertEqual(context.exception.detail.code, 'inactive_user')


class LoginSerializerTestCase(TestCase, ManagesUsers):
    def test_validate(self):
        request = Request(HttpRequest())
        user = self.create_user()
        serializer = LoginSerializer(context={'request': request})
        attrs = serializer.validate({'email': user.email, 'password': 'password'})
        self.assertIn('user', attrs)
        self.assertIsInstance(user, User)

    def test_validate_raises_validation_error_when_credentials_invalid(self):
        request = Request(HttpRequest())
        user = self.create_user()
        serializer = LoginSerializer(context={'request': request})
        with self.assertRaises(ValidationError) as context:
            serializer.validate({'email': user.email, 'password': 'invalid'})


class LoginViewTestCase(TestCase, ManagesUsers):
    def setUp(self):
        self.client = APIClient()
        self.user = self.create_user()

    def test_login(self):
        response: Response = self.client.post(reverse('login'), {
            'email': self.user.email,
            'password': 'password'
        }, format='json', HTTP_USER_AGENT='unittest')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('token', response.data)
        self.assertIn('user', response.data)

        # check user can access protected area
        url = reverse('check-auth')
        token = response.data['token']
        resp: Response = self.client.get(url, {}, HTTP_AUTHORIZATION=f'Bearer {token}')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_login_failed_with_invalid_password(self):
        response: Response = self.client.post(reverse('login'), {
            'email': 'user@localhost',
            'password': 'invalid'
        }, format='json', HTTP_USER_AGENT='unittest')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class LogoutViewTestCase(TestCase, ManagesUsers):
    def setUp(self):
        self.client = APIClient()
        self.user = self.create_user()
        self.token = issue_token(self.user)

    def test_logout(self):
        self.client.post(reverse('logout'), {}, HTTP_AUTHORIZATION=f'Bearer {self.token}')

        # cannot access protected area anymore
        response: Response = self.client.get(reverse('check-auth'), {}, HTTP_AUTHORIZATION=f'Bearer {self.token}')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.assertIsNone(fetch_token(self.token))


class ForgotPasswordViewTestCase(TestCase, ManagesUsers):
    def test_request_password_reset(self):
        user = self.create_user()
        response = self.client.post(reverse('forgot-password'), {'email': user.email})
        self.assertEquals(response.status_code, 200)

        pr_exists = PasswordReset.objects.filter(user=user).exists()
        self.assertTrue(pr_exists)

        user.refresh_from_db()
        code = PasswordReset.objects.filter(user=user).get()
        self.assertEquals(len(mail.outbox), 1)
        letter = mail.outbox[0]
        url = abs_reverse('public_reset_password', kwargs={'token': str(code.token)})
        self.assertIn(url, letter.body)

    def test_request_password_fails_for_invalid_email(self):
        response = self.client.post(reverse('forgot-password'), {'email': 'non@existing.com'})
        self.assertEquals(response.status_code, 400)


class ResetPasswordViewTestCase(TestCase, ManagesUsers):
    def test_changes_password(self):
        user = self.create_user()
        code = PasswordReset.objects.create(user=user, token=uuid.uuid4())
        response = self.client.post(reverse('reset-password'), {
            'email':    user.email,
            'password': '123456',
            'token':    code.token,
        })
        self.assertEquals(response.status_code, 200)

        pr_exists = PasswordReset.objects.filter(user=user).exists()
        self.assertFalse(pr_exists)

        user.refresh_from_db()
        self.assertTrue(user.check_password('123456'))

    def test_change_password_fails_for_invalid_email(self):
        response = self.client.post(reverse('reset-password'), {'email': 'non@existing.com'})
        self.assertEquals(response.status_code, 400)

    def test_change_password_fails_for_invalid_token(self):
        user = self.create_user()
        response = self.client.post(reverse('reset-password'), {'email': user.email, 'token': 'invalid'})
        self.assertEquals(response.status_code, 400)


class ChangePasswordViewTestCase(TestCase, ManagesUsers):
    def test_ok(self):
        user = self.create_user(password='111111')
        self.client.force_login(user)
        response = self.client.post(reverse('change-password'), data={
            'current_password': '111111',
            'password':         '222222'
        })
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        user.refresh_from_db()
        self.assertTrue(user.check_password('222222'))

    def test_fails_when_invalid_current_password(self):
        user = self.create_user(password='111111')
        self.client.force_login(user)
        response = self.client.post(reverse('change-password'), data={
            'current_password': '333333',
            'password':         '222222'
        })
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

        user.refresh_from_db()
        self.assertTrue(user.check_password('111111'))  # password still the same

    def test_fails_when_no_password(self):
        user = self.create_user(password='111111')
        self.client.force_login(user)
        response = self.client.post(reverse('change-password'), data={
            'current_password': '333333',
            'password':         ''
        })
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

        user.refresh_from_db()
        self.assertTrue(user.check_password('111111'))  # password still the same
