from django.utils.translation import ugettext_lazy as _

from arasari.core.urls import abs_reverse
from arasari.mail.mail import Mail


class VerifyEmailMail(Mail):
    subject = _('Verify your email address')

    def __init__(self, user, code):
        super().__init__()

        self.user = user
        self.code = code

    def build(self):
        url = abs_reverse('security_confirm_email', kwargs={'code': self.code})
        self.text(_('Please click the button below to verify your email address.'))
        self.button(_('Verify Email Address'), url)
        self.text(_('If you did not create an account, no further action is required.'))


class ForgotPasswordMail(Mail):
    subject = _('Reset Password Notification')

    def __init__(self, user, code):
        super().__init__()
        self.user = user
        self.code = code

    @property
    def greeting(self):
        return _('Hi, %(first_name)s!') % {'first_name': self.user.first_name}

    def build(self):
        url = abs_reverse('public_reset_password', kwargs={'token': self.code.token})
        self.text(_('You are receiving this email because we received a password reset request for your account.'))
        self.button(_('Reset Password'), url)
        self.text(_('If you did not request a password reset, no further action is required.'))
