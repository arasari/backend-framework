from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class ApiToken(models.Model):
    """ Api token model. This token is UUID4 based. """

    token = models.CharField(
        max_length=256, db_index=True, unique=True, editable=False, help_text='a sha512 hashed token'
    )
    created_at = models.DateTimeField(auto_now_add=True)
    expires_at = models.DateTimeField(null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='api_tokens')


class PasswordReset(models.Model):
    token = models.UUIDField()
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
