import django.dispatch

# fired after successful authentication (happens when user actually logs in)
user_interactive_login = django.dispatch.Signal(providing_args=['user', 'request'])

# fired after failed authentication
user_login_error = django.dispatch.Signal(providing_args=['message', 'request', 'exception'])

# fired after user has been authenticated via token (happens on every request)
user_login_success = django.dispatch.Signal(providing_args=['token', 'request'])

# fired after user has been registered
user_registered = django.dispatch.Signal(providing_args=['user', 'request'])
