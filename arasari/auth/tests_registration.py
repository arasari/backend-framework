import uuid
from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APIRequestFactory
from unittest import mock
from unittest.mock import MagicMock

from arasari.auth.views import BaseRegisterView, RequiresEmailConfirmation
from arasari.core.urls import abs_reverse
from arasari.users.test_utils import ManagesUsers

User = get_user_model()


class VerifyEmailViewTestCase(TestCase, ManagesUsers):
    def test_confirms_email(self):
        token = uuid.uuid4()
        user = self.create_user(confirmation_code=token)
        response = self.client.get(reverse('security_confirm_email', kwargs={'code': str(token)}))
        self.assertEquals(response.status_code, status.HTTP_302_FOUND)
        self.assertEquals(response['Location'], reverse('public_login'))
        user.refresh_from_db()

        self.assertTrue(user.is_confirmed)
        self.assertIsNone(user.confirmation_code)
        self.assertIsNotNone(user.confirmed_at)

    def test_fails_if_token_invalid(self):
        token = uuid.uuid4()
        response = self.client.get(reverse('security_confirm_email', kwargs={'code': str(token)}))
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)


class ResendEmailConfirmationViewTestCase(TestCase, ManagesUsers):
    def test_sends_email(self):
        user = self.create_user()
        self.client.force_login(user)
        response = self.client.post(reverse('security_resend_email'))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        user.refresh_from_db()
        self.assertEqual(1, len(mail.outbox))
        letter = mail.outbox[0]
        url = abs_reverse('security_confirm_email', kwargs={'code': str(user.confirmation_code)})
        self.assertIn(url, letter.body)

    def test_fails_if_confirmed(self):
        user = self.create_user(confirmed_at=timezone.now())
        self.client.force_login(user)
        response = self.client.post(reverse('security_resend_email'))
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

        user.refresh_from_db()
        self.assertEquals(0, len(mail.outbox))


class DefaultRegisterViewTestCase(TestCase, ManagesUsers):
    def test_register_requires_email(self):
        class RegisterView(RequiresEmailConfirmation, BaseRegisterView):
            pass

        view = RegisterView.as_view()
        factory = APIRequestFactory()

        # CASE 1: email was not send -- validation error
        request = factory.post(reverse('users_register'), {
            'first_name': self.get_faker().first_name(),
            'last_name': self.get_faker().last_name(),
            'password': '123456',
            'terms': True
        })
        response = view(request)
        self.assertEquals(response.status_code, 400)

        # CASE 2: email was send -- ok
        email = self.get_faker().email()
        request = factory.post(reverse('users_register'), {
            'first_name': self.get_faker().first_name(),
            'last_name': self.get_faker().last_name(),
            'password': '123456',
            'email': email,
            'terms': True
        })
        response = view(request)
        self.assertEquals(response.status_code, 200)

        # user has to be registered now
        user = User.objects.filter(email=email).get()
        self.assertTrue(user.is_active)
        self.assertEquals(len(mail.outbox), 1)
        letter = mail.outbox[0]
        url = abs_reverse('security_confirm_email', kwargs={'code': str(user.confirmation_code)})
        self.assertIn(url, letter.body)
