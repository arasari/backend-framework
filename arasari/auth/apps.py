from django.apps import AppConfig


class DefaultConfig(AppConfig):
    name = 'arasari.auth'
    label = 'arasari_auth'
