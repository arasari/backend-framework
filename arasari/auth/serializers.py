from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework import exceptions, serializers
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.validators import UniqueValidator

from .authentication import authenticate
from .models import PasswordReset

User = get_user_model()


class LoginSerializer(serializers.Serializer):
    """ Login flow serializer."""

    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        """ Validates credentials. """

        email = attrs.get('email')
        password = attrs.get('password')
        request = self.context['request']

        try:
            user = authenticate(request, email=email, password=password)
        except AuthenticationFailed as ex:
            # raise error here so DRF can render a nice error message for the client
            raise exceptions.ValidationError(ex)
        else:
            attrs['user'] = user
            return attrs


class DefaultUserSerializer(serializers.ModelSerializer):
    """
    Standard user serializer.

    Includes only commonly used fields.
    """

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name',)


class ChecksEmailExists:
    def validate_email(self, value):
        try:
            User.objects.filter(email=value).get()
        except User.DoesNotExist:
            raise serializers.ValidationError(_('User with given email was not found.'))
        else:
            return value


class ForgotPasswordSerializer(ChecksEmailExists, serializers.Serializer):
    email = serializers.EmailField(required=True, write_only=True)

    class Meta:
        fields = ('email',)


class ResetPasswordSerializer(ChecksEmailExists, serializers.Serializer):
    email = serializers.EmailField(required=True, write_only=True)
    password = serializers.CharField(required=True, write_only=True, min_length=6)
    token = serializers.CharField(required=True, write_only=True)

    class Meta:
        fields = ('email', 'password')

    def validate_token(self, value):
        if not PasswordReset.objects.filter(token=value).exists():
            raise serializers.ValidationError(_('Invalid password reset token.'))
        return value


class ChangePasswordSerializer(serializers.Serializer):
    current_password = serializers.CharField(required=True)
    password = serializers.CharField(min_length=6, required=True)

    def validate_current_password(self, value):
        user = self.context['request'].user
        if not user.check_password(value):
            raise serializers.ValidationError(_('Invalid current password.'))
        return value


class BaseRegisterSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    email = serializers.EmailField(
        required=True,
        validators=[
            UniqueValidator(queryset=User.objects.all(), message=_('This email is already taken.'))
        ]
    )
    password = serializers.CharField(required=True, min_length=6)
    terms = serializers.BooleanField(required=True)

    class Meta:
        model = User
        exclude = ('username',)

    def validate_terms(self, value):
        if not value:
            raise serializers.ValidationError(_('You have to accept our terms.'))
        return value


class RegisterSerializer(BaseRegisterSerializer): pass
