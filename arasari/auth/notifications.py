from arasari.notifications.notification import BaseNotification
from .mails import VerifyEmailMail


class ConfirmRegistration(BaseNotification):
    via = ['mail']

    def to_mail(self, user):
        return VerifyEmailMail(user, user.confirmation_code)
