import binascii
import time

import hashlib
import jwt
from datetime import timedelta
from django.conf import settings
from django.contrib.auth import get_user_model, authenticate as django_authenticate
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication as BaseTokenAuthentication
from rest_framework.request import Request
from typing import Union, Optional

from .signals import user_login_error, user_interactive_login, user_login_success
from .models import ApiToken

User = get_user_model()


class TokenAuthentication(BaseTokenAuthentication):
    """ Validates incoming token. """

    model = ApiToken
    keyword = 'Bearer'

    def authenticate_credentials(self, key) -> Union[User, str]:
        """ Authenticates user via API token. """

        model = self.get_model()
        try:
            key = get_token_hash(key.encode())
            token = model.objects.select_related('user').get(token=key)
        except model.DoesNotExist:
            raise exceptions.AuthenticationFailed(_('auth.errors.invalid_token'), code='invalid_token')

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed(_('auth.errors.inactive_user'), code='inactive_user')

        # let other apps to know that user was authenticated
        user_login_success.send(sender=__class__, token=token)

        return token.user, token


def issue_token(user: User, lifetime: int = 30) -> str:
    """ Issues an API token.

    Note that the token is stored in the database so it is not stateless.

    :param user: User
    :param lifetime: A token lifetime (in days)
    :return: str
    """

    payload = {
        'id': user.id,
        'email': user.email,
        'issued': time.time()
    }
    value = jwt.encode(payload, settings.SECRET_KEY, 'HS256').decode()
    expiration_date = timezone.now() + timedelta(days=lifetime)
    token_hash = get_token_hash(value)

    ApiToken.objects.create(
        user=user,
        token=token_hash,
        expires_at=expiration_date
    )
    return value


def fetch_token(token: str) -> Optional[ApiToken]:
    """ Fetches a token object using plain token string. """

    try:
        token_hash = get_token_hash(token)
        obj = ApiToken.objects.filter(token=token_hash).get()
    except ApiToken.DoesNotExist:
        return None
    else:
        return obj


def authenticate(request: Request, email: str, password: str) -> User:
    """ Authenticates user using identity (a model field) and password.

    Sends `user_login_success` sent when credentials are valid.
    Sends `user_login_error` sent when credentials are invalid.

    Raise AuthenticationFailed to abort authentication flow and prevent user from logging in.

    :param request: A request instance.
    :param email: A model field. Commonly email or username.
    :param password: A user's password.
    :raises: AuthenticationFailed
    :return: User instance.
    """
    username_field = get_user_model().USERNAME_FIELD
    kwargs = {
        username_field: email,
        'password': password,
    }

    user = django_authenticate(request, **kwargs)
    if not user:
        user_login_error.send(sender=__name__, message='auth.errors.invalid_credentials')
        raise exceptions.AuthenticationFailed(_('auth.errors.invalid_credentials'), code='invalid_credentials')

    user_interactive_login.send(sender=__name__, user=user, request=request)
    return user


def get_token_hash(token: str) -> str:
    """
    Hashes token using sha512.

    :param token: str
    :return: str A hashed token.
    """
    if isinstance(token, str):
        token = token.encode()

    hash = hashlib.sha512()
    hash.update(token)
    return binascii.hexlify(hash.digest()).decode()
