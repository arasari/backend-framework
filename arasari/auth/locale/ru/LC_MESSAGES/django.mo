��          �      L      �     �  @   �  G        ^     x  ;   �     �     �     �  $        ?     T  [   n     �      �     	     #     C     ]  )   ^  g   �  g   �  -   X  7   �  b   �     !  )   =  g   g  0   �  3    	  3   4	  �   h	  S   
  '   X
  #   �
  7   �
     �
                              	                             
                                         Hi, %(first_name)s! If you did not create an account, no further action is required. If you did not request a password reset, no further action is required. Invalid current password. Invalid password reset token. Please click the button below to verify your email address. Reset Password Reset Password Notification This email is already taken. User with given email was not found. Verify Email Address Verify your email address You are receiving this email because we received a password reset request for your account. You have to accept our terms. Your confirmation code: %(code)s auth.errors.inactive_user auth.errors.invalid_credentials auth.errors.invalid_token Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-12-02 17:19+0300
PO-Revision-Date: 2018-12-02 17:21+0300
Last-Translator: Alex Oleshkevich <alex.oleshkevich@gmail.com>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 2.2
 Здравствуйте, %(first_name)s! Если это были не вы, то просто проигнорируйте это письмо. Если это были не вы, то просто проигнорируйте это письмо. Неверный текущий пароль. Неверный код для смены пароля. Намжите на кнопку ниже для подтверждения вашей почты. Сменить пароль Запрос на сброс пароля Этот адрес почты уже используется другим пользователем. Учетная запись не найдена. Подтвердите ваш адрес почты Подтвердите ваш адрес почты Вы получили это письмо так как кто-то запросил смену пароля для вашей учетной записи. Необходимо согласие с правилами пользования. Проверочный код: %(code)s Аккаунт не активен. Неверные данные пользователя. Неверный токен. 