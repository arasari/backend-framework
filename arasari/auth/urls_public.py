from django.urls import path

from . import views

urlpatterns = [
    path('login/', views.LoginView.as_view(), name='public_login'),
    path('password/reset/<str:token>', views.LoginView.as_view(), name='public_reset_password'),
]
