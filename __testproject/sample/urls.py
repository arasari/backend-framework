from django.urls import include, path

from arasari.auth.views import DefaultRegisterView

urlpatterns = [
    path('api/', include([
        path('', include('arasari.auth.urls')),
        path('', include('arasari.auth.urls_public')),
        path('', include('arasari.users.urls')),
        path('', include('arasari.messengers.urls')),
        path('', include('arasari.content.urls')),
        path('', include('arasari.content.urls_pages')),
        path('', include('arasari.faq.urls')),
        path('register/', DefaultRegisterView.as_view(), name='users_register'),
    ])),
]
