#!/usr/bin/env python

import os

from setuptools import find_packages, setup

'''
Learn more about packaging here:
https://packaging.python.org/distributing/#setup-args
'''

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='arasari',
    version='1.0.1',
    description='Arasari backend library',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Django>=2.1',
        'psycopg2-binary',
        'faker',
        'coverage',
        'flake8',
        'pep8',
        'django-cleanup',
        'django-cors-headers',
        'django-stdimage',
        'djangorestframework',
        'faker',
        'pyjwt',
        'python-telegram-bot',
        'viberbot',
        'texttable',
        'wheel',
        'humanize',
        'twine',
        'sphinx',
        'sphinx_rtd_theme',
    ],
    url='https://gitlab.com/arasari/backend-framework',
    classifiers=[
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: Other/Proprietary License',
        'Operating System :: OS Independent',
        'Topic :: Software Development'
    ],
    author='Alex Oleshkevich',
    author_email='alex.oleshkevich@gmail.com',
    license='License :: Other/Proprietary License',
)
